package digilibapp.morfin.com.digilibapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import digilibapp.morfin.com.digilibapp.firebase_token_service.FirebaseToken;
import digilibapp.morfin.com.digilibapp.models.Mahasiswa;
import digilibapp.morfin.com.digilibapp.sharedpreference.SharedPrefManager;
import digilibapp.morfin.com.digilibapp.url.URLs;
import digilibapp.morfin.com.digilibapp.volley.VolleySingleton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {
    TextView forgotPassword, appTitle;
    EditText nim, password;
    ImageView logo;
    String myFirebaseTokenValue = "";
    SweetAlertDialog sweetAlertDialog;
    FirebaseToken firebaseToken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (SharedPrefManager.getInstance(LoginActivity.this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, MainActivityDrawer.class));
        } else {
            firebaseToken  = new FirebaseToken();
            sweetAlertDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

            nim    = (EditText)findViewById(R.id.nim);
            password    = (EditText)findViewById(R.id.password);
            forgotPassword    = (TextView) findViewById(R.id.forgotPassword);
            appTitle = (TextView) findViewById(R.id.appTitle);
            logo = findViewById(R.id.logo);
            starterPack();
        }
    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS


    public boolean checkInternet(){
        boolean connectStatus = true;
        ConnectivityManager ConnectionManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true ) {
            connectStatus = true;
        }
        else {
            connectStatus = false;
        }
        return connectStatus;
    }

    private void starterPack(){
        logo.setImageDrawable(getResources().getDrawable(R.drawable.applogo));
        if (Build.VERSION.SDK_INT >= 21) {
            nim.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_icon_user, 0,0,0);
            nim.setCompoundDrawablePadding(10);
            password.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_icon_key, 0,0,0);
            password.setCompoundDrawablePadding(10);
        }
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_GO){
                    if (checkInternet()) {
                        if (nim.getText().toString().isEmpty()){
                            nim.setError("NIM tidak boleh kosong");
                            nim.requestFocus();
                        } else if (password.getText().toString().isEmpty()){
                            password.setError("Password tidak boleh kosong");
                            password.requestFocus();
                        } else {
                            getToken();
                            final String nims = nim.getText().toString().trim();
                            final String passwords = password.getText().toString().trim();
                            onLogin(nims, passwords);
                            return true;
                        }
                    } else {
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setContentText("Nyalakan data seluler dahulu")
                                .setConfirmText("Okay")
                                .show();
                    }
                }
                return false;
            }
        });
    }

    private void onLogin(final String nim, final String password){
        sweetAlertDialog.getProgressHelper().setBarColor(R.color.colorPrimary);
        sweetAlertDialog.setTitleText("Autentikasi...");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.auth_mahasiswa,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String id_mahasiswa, nim, password, nama, jurusan, token_android;
                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);
                            //getting the user from the response
                            JSONArray results = obj.getJSONArray("results");
                            String status = obj.getString("status").toString();
                            sweetAlertDialog.dismiss();
                            if (status.equals("OK")) {
                                JSONObject data = results.getJSONObject(0);
                                id_mahasiswa = data.getString("id_mahasiswa");
                                nim = data.getString("nim");
                                password = data.getString("password");
                                nama = data.getString("nama");
                                jurusan = data.getString("jurusan");
                                Mahasiswa mahasiswa = new Mahasiswa(
                                        id_mahasiswa, nim, password, nama, jurusan, myFirebaseTokenValue
                                );
                                SharedPrefManager.getInstance(LoginActivity.this).onLogin(mahasiswa);
                                finish();
                                startActivity(new Intent(LoginActivity.this, MainActivityDrawer.class));
                            } else if(status.equals("NULL")){
                                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setContentText("NIM atau katasandi salah")
                                        .setConfirmText("Okay")
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.printf("Error : " + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("nim", nim);
                params.put("password", password);
                params.put("token", myFirebaseTokenValue);
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void getToken(){
        myFirebaseTokenValue = firebaseToken.refreshedToken();
//        Toast.makeText(getApplicationContext(),"Token : "+myFirebaseTokenValue, Toast.LENGTH_SHORT).show();
    }

}
