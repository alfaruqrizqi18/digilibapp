package digilibapp.morfin.com.digilibapp.detail_activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.ticket_view.TicketView;
import digilibapp.morfin.com.digilibapp.url.URLs;
import digilibapp.morfin.com.digilibapp.volley.VolleySingleton;
import mehdi.sakout.fancybuttons.FancyButton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailTransaksiPeminjamanActivity extends AppCompatActivity {
    String val_id_transaksi;
    TextView judul_buku, tgl_transaksi, tgl_kembali, buku_dikembalikan, total_denda, tv1;
    FancyButton status_transaksi;
    ImageView thumbnail;
    Button btn_close;
    ProgressBar progressBar;
    TicketView layout_ticket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaksi_peminjaman);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Transaksi Peminjaman");

        val_id_transaksi = getIntent().getStringExtra("id_transaksi").toString();

        layout_ticket = (TicketView)findViewById(R.id.layout_ticket);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        tv1 = (TextView)findViewById(R.id.tv1);
        thumbnail = (ImageView)findViewById(R.id.thumbnail);
        judul_buku = (TextView)findViewById(R.id.judul_buku);
        tgl_transaksi = (TextView)findViewById(R.id.tgl_transaksi);
        tgl_kembali = (TextView)findViewById(R.id.tgl_kembali);
        buku_dikembalikan = (TextView)findViewById(R.id.buku_dikembalikan);
        total_denda = (TextView)findViewById(R.id.total_denda);
        status_transaksi = (FancyButton)findViewById(R.id.status_transaksi);;
        btn_close = (Button)findViewById(R.id.btn_close_window);
        starterPack();
    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void starterPack(){
        showDetailPeminjaman();
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void beforeLoad(){
        progressBar.setVisibility(View.VISIBLE);
        layout_ticket.setVisibility(View.GONE);
        tv1.setVisibility(View.GONE);
        btn_close.setVisibility(View.GONE);
    }

    private void showResult(){
        progressBar.setVisibility(View.GONE);
        layout_ticket.setVisibility(View.VISIBLE);
        tv1.setVisibility(View.VISIBLE);
        btn_close.setVisibility(View.VISIBLE);
    }

    private void showDetailPeminjaman() {
        beforeLoad();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.detail_transaction_peminjaman+val_id_transaksi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showResult();
                        try {
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Date tgl_transaksi_date, tgl_kembali_date, tgl_buku_dikembalikan_date = new Date();
                            for (int i = 0; i < results.length(); i++) {
                                JSONObject data = results.getJSONObject(i);
                                try {
                                    tgl_transaksi_date = simpleDateFormat.parse(data.getString("tgl_transaksi".toString().trim()));
                                    tgl_kembali_date = simpleDateFormat.parse(data.getString("tgl_kembali_buku".toString().trim()));
                                    tgl_buku_dikembalikan_date = simpleDateFormat.parse(data.getString("tgl_buku_dikembalikan".toString().trim()));
                                    simpleDateFormat.applyPattern("d MMM yyyy");
                                    tgl_transaksi.setText(simpleDateFormat.format(tgl_transaksi_date));
                                    tgl_kembali.setText(simpleDateFormat.format(tgl_kembali_date));

                                    if (data.getString("tgl_buku_dikembalikan".toString().trim()).contains("0000")){
                                        buku_dikembalikan.setText("Belum dikembalikan");
                                    } else {
                                        buku_dikembalikan.setText(simpleDateFormat.format(tgl_buku_dikembalikan_date));
                                    }

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                total_denda.setText(data.getString("total_denda"));
                                judul_buku.setText(data.getString("judul_buku"));
                                Glide.with(getApplicationContext()).load(URLs.base_url_web.toString() + data.getString("thumbnail")).into(thumbnail);

                                if (data.getString("status_transaksi").equals("peminjaman_sedang_berlangsung")) {
                                    status_transaksi.setText("Berlangsung");
                                    status_transaksi.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                                    status_transaksi.setFocusBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

                                } else if (data.getString("status_transaksi").equals("peminjaman_melewati_batas")) {
                                    status_transaksi.setText("Lewati Batas");
                                    status_transaksi.setBackgroundColor(getResources().getColor(R.color.red));
                                    status_transaksi.setFocusBackgroundColor(getResources().getColor(R.color.dark_red));

                                } else if (data.getString("status_transaksi").equals("peminjaman_telah_berakhir")) {
                                    status_transaksi.setText("Berakhir");
                                    status_transaksi.setBackgroundColor(getResources().getColor(R.color.green));
                                    status_transaksi.setFocusBackgroundColor(getResources().getColor(R.color.dark_green));
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
