package digilibapp.morfin.com.digilibapp.nav_menu_activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.adapter.AdapterAllNotifikasi;
import digilibapp.morfin.com.digilibapp.models.Notifikasi;
import digilibapp.morfin.com.digilibapp.sharedpreference.SharedPrefManager;
import digilibapp.morfin.com.digilibapp.url.URLs;
import digilibapp.morfin.com.digilibapp.volley.VolleySingleton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NotifikasiActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private AdapterAllNotifikasi adapterAllNotifikasi;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifikasi);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pemberitahuan");
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        showAllNotifikasi();
    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void showAllNotifikasi(){
        progressBar.setVisibility(View.VISIBLE);
        final String nim = SharedPrefManager.getInstance(NotifikasiActivity.this).showSession().get("NIM").toString();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.all_notification_by_nim + nim,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<Notifikasi> dataNotifikasi = new ArrayList<>();
                        try {
                            dataNotifikasi.clear();
                            recyclerView.setLayoutManager(null);
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Date tgl_notifikasi = new Date();
                            for (int i = 0; i < results.length(); i++) {
                                JSONObject data = results.getJSONObject(i);
                                Notifikasi notifikasi = new Notifikasi();
                                notifikasi.setId_notifikasi(data.getString("id_notifikasi"));
                                notifikasi.setJudul_notifikasi(data.getString("judul_notifikasi"));
                                notifikasi.setPesan_notifikasi(data.getString("pesan_notifikasi"));
                                notifikasi.setTgl_notifikasi(data.getString("tgl_notifikasi"));
                                dataNotifikasi.add(notifikasi);
                                Log.d("Notifikasi ", String.valueOf(notifikasi));
                            }
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            adapterAllNotifikasi = new AdapterAllNotifikasi(getApplicationContext(), dataNotifikasi);
                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setAdapter(adapterAllNotifikasi);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
