package digilibapp.morfin.com.digilibapp.url;

/**
 * Created by Morfin on 1/25/2018.
 */

public class URLs {

    //base url
//    public static final String base_url = "https://digilibapps.000webhostapp.com/api/";
//    public static final String base_url_web = "https://digilibapps.000webhostapp.com/";
    public static final String base_url = "http://192.168.43.184/digilib/api/";
    public static final String base_url_web = "http://192.168.43.184/digilib/";

    //auth login
    public static final String auth_mahasiswa = base_url+"auth_mahasiswa/?method=post";

    //change_password
    public static final String change_password = base_url+"change_password/";

    //profile
    public static final String profile_url = base_url +"profile/?nim=";

    //tampil buku atau laporan sebelum melakukan searching
    public static final String landingpage_books = base_url+"buku/?method=landingpage";
    public static final String landingpage_books_full_filter = base_url+"buku/?method=landingpage_full_filter&";
    public static final String landingpage_laporan = base_url+"laporan/?method=landingpage";

    //pencarian buku dan laporan
    public static final String search_url_books_1 = base_url+"buku/?method=get&judul=";
    public static final String search_url_books_1_full_filter = base_url+"buku/?method=full_filter&";
    public static final String search_url_laporan_1 = base_url+"laporan/?method=get&judul=";

    //# get stok buku dan detail buku
    public static final String get_stock_and_detail_book = base_url+"buku/?method=get_stock&id_buku=";



    //=====PEMESANAN=====//
    //1. proses store transaksi pemesanan
    public static final String store_transaction_proccess = base_url + "pemesanan/?method=post";

    //2. detail transaksi pemesanan
    public static final String detail_transaction = base_url + "pemesanan/?method=detail&id_pemesanan=";

    //3. tampil semua pemesanan berdasarkan nim login
    public static final String all_transaction_pemesanan_by_nim = base_url + "pemesanan/?method=show_pemesanan_by_nim&nim=";

    //4. batalkan pemesanan
    public static final String cancel_transaction = base_url + "pemesanan/?method=cancel_pemesanan";

    //5. tampil pemesanan berdasarkan status
    public static final String all_transaction_pemesanan_by_nim_filter_success = base_url + "pemesanan/?method=show_pemesanan_by_nim_filter&status=success&nim=";
    public static final String all_transaction_pemesanan_by_nim_filter_waiting = base_url + "pemesanan/?method=show_pemesanan_by_nim_filter&status=waiting&nim=";
    public static final String all_transaction_pemesanan_by_nim_filter_expired = base_url + "pemesanan/?method=show_pemesanan_by_nim_filter&status=expired&nim=";


    //=====PEMINJAMAN=====//
    //1. tampil semua peminjaman berdasarkan nim login
    public static final String all_transaction_peminjaman_by_nim = base_url + "peminjaman/?method=show_peminjaman_by_nim&nim=";

    //2. detail transaksi peminjaman
    public static final String detail_transaction_peminjaman = base_url + "peminjaman/?method=detail&id_transaksi=";

    //3. tampil peminjaman berdasarkan status
    public static final String all_transaction_peminjaman_by_nim_berlangsung = base_url + "peminjaman/?method=show_peminjaman_by_nim_filter&status=peminjaman_sedang_berlangsung&nim=";
    public static final String all_transaction_peminjaman_by_nim_berakhir = base_url + "peminjaman/?method=show_peminjaman_by_nim_filter&status=peminjaman_telah_berakhir&nim=";
    public static final String all_transaction_peminjaman_by_nim_lewat_batas = base_url + "peminjaman/?method=show_peminjaman_by_nim_filter&status=peminjaman_melewati_batas&nim=";


    //=====NOTIFIKASI====//
    //1. tampil semua notifikasi berdasarkan nim login
    public static final String all_notification_by_nim = base_url + "notifikasi/?type=1&nim=";


}
