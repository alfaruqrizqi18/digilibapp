package digilibapp.morfin.com.digilibapp.models;

/**
 * Created by Morfin on 3/9/2018.
 */

public class Notifikasi {

    String id_notifikasi, nim, judul_notifikasi, pesan_notifikasi, token_android, tgl_notifikasi;

    public String getId_notifikasi() {
        return id_notifikasi;
    }

    public void setId_notifikasi(String id_notifikasi) {
        this.id_notifikasi = id_notifikasi;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getJudul_notifikasi() {
        return judul_notifikasi;
    }

    public void setJudul_notifikasi(String judul_notifikasi) {
        this.judul_notifikasi = judul_notifikasi;
    }

    public String getPesan_notifikasi() {
        return pesan_notifikasi;
    }

    public void setPesan_notifikasi(String pesan_notifikasi) {
        this.pesan_notifikasi = pesan_notifikasi;
    }

    public String getToken_android() {
        return token_android;
    }

    public void setToken_android(String token_android) {
        this.token_android = token_android;
    }

    public String getTgl_notifikasi() {
        return tgl_notifikasi;
    }

    public void setTgl_notifikasi(String tgl_notifikasi) {
        this.tgl_notifikasi = tgl_notifikasi;
    }
}
