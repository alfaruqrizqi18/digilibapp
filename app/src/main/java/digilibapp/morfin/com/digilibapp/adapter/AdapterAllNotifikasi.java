package digilibapp.morfin.com.digilibapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.detail_activity.DetailNotifikasiActivity;
import digilibapp.morfin.com.digilibapp.models.Notifikasi;

/**
 * Created by Morfin on 3/9/2018.
 */

public class AdapterAllNotifikasi extends RecyclerView.Adapter<AdapterAllNotifikasi.MyViewHolder> {
    private List<Notifikasi> notifikasiList;
    private Context mContext;

    public AdapterAllNotifikasi(Context mContext, List<Notifikasi> notifikasiList) {
        this.mContext = mContext;
        this.notifikasiList = notifikasiList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView judul_notifikasi, pesan_notifikasi, tgl_notifikasi;
        LinearLayout linearLayout;
        FrameLayout card_edge;
        public MyViewHolder(View view) {
            super(view);
            linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
            judul_notifikasi = (TextView) view.findViewById(R.id.judul_notifikasi);
            pesan_notifikasi = (TextView) view.findViewById(R.id.pesan_notifikasi);
            tgl_notifikasi = (TextView) view.findViewById(R.id.tgl_notifikasi);
            card_edge = (FrameLayout) view.findViewById(R.id.card_edge);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notifikasi_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Notifikasi notifikasi = notifikasiList.get(position);
        int get_id_notifikasi = Integer.parseInt(notifikasi.getId_notifikasi());
        if (get_id_notifikasi%2 == 0) {
            holder.card_edge.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.card_edge.setBackgroundColor(mContext.getResources().getColor(R.color.green));
        }
        holder.judul_notifikasi.setText(notifikasi.getJudul_notifikasi());
        holder.pesan_notifikasi.setText(notifikasi.getPesan_notifikasi());
        holder.tgl_notifikasi.setText("diterima pada "+notifikasi.getTgl_notifikasi());
        holder.judul_notifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_detail_notifikasi(
                        notifikasi.getId_notifikasi(),
                        notifikasi.getJudul_notifikasi(),
                        notifikasi.getPesan_notifikasi(),
                        notifikasi.getTgl_notifikasi()
                );
            }
        });
        holder.pesan_notifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_detail_notifikasi(
                        notifikasi.getId_notifikasi(),
                        notifikasi.getJudul_notifikasi(),
                        notifikasi.getPesan_notifikasi(),
                        notifikasi.getTgl_notifikasi()
                );
            }
        });
        holder.tgl_notifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_detail_notifikasi(
                        notifikasi.getId_notifikasi(),
                        notifikasi.getJudul_notifikasi(),
                        notifikasi.getPesan_notifikasi(),
                        notifikasi.getTgl_notifikasi()
                );
            }
        });
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_detail_notifikasi(
                        notifikasi.getId_notifikasi(),
                        notifikasi.getJudul_notifikasi(),
                        notifikasi.getPesan_notifikasi(),
                        notifikasi.getTgl_notifikasi()
                );
            }
        });
    }

    private void open_detail_notifikasi(String id_notifikasi, String judul_notifikasi, String pesan_notifikasi, String tgl_notifikasi){
        Intent intent = new Intent(mContext, DetailNotifikasiActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id_notifikasi", id_notifikasi);
        intent.putExtra("judul_notifikasi", judul_notifikasi);
        intent.putExtra("pesan_notifikasi", pesan_notifikasi);
        intent.putExtra("tgl_notifikasi", tgl_notifikasi);
        mContext.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return notifikasiList.size();
    }
}
