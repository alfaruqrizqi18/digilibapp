package digilibapp.morfin.com.digilibapp.firebase_token_service;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

/**
 * Created by Morfin on 2/3/2018.
 */

public class FirebaseToken {
    String myFirebaseToken;
    FirebaseInstanceId firebaseInstanceId;

    public void firebaseInitialize(){
        firebaseInstanceId = FirebaseInstanceId.getInstance();
    }
    public String refreshedToken(){
        firebaseInitialize();
        String token = firebaseInstanceId.getToken();
        return token;
    }

    public void deleteFirebaseToken(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                firebaseInitialize();
                try {
                    firebaseInstanceId.deleteInstanceId();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                refreshedToken();
            }
        }).start();
    }
}
