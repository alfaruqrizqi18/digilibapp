package digilibapp.morfin.com.digilibapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import org.angmarch.views.NiceSpinner;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FilterActivity extends AppCompatActivity {
    NiceSpinner kategoriDropdown, gmd;
    String categoriesLocation, subyek_val, isbn_val, pengarang_val, gmd_val;
    FancyButton buttonCari;
    boolean isFullFilter;
    EditText pengarang, subyek, isbn;
    int REQUEST_CODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        //toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Filter");

        kategoriDropdown = (NiceSpinner) findViewById(R.id.nice_spinner);
        gmd = (NiceSpinner) findViewById(R.id.gmd);
        pengarang = (EditText) findViewById(R.id.pengarang);
        subyek = (EditText) findViewById(R.id.subyek);
        isbn = (EditText) findViewById(R.id.isbn);
        buttonCari = (FancyButton) findViewById(R.id.buttonCari);
        starterPack();

    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS


    private void starterPack(){
        gmd_val = "";
        categoriesLocation = "Buku";
        dropdownSetup();
        hideContentCategoryBuku();
        buttonCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (kategoriDropdown.getSelectedIndex() == 0) {
                    Toast.makeText(getApplicationContext(), "Mohon pilih kategori dahulu", Toast.LENGTH_SHORT).show();
                } else {
                    sendResults();
                }
            }
        });
    }

    private void showContentCategoryBuku(){
        findViewById(R.id.cat_buku).setVisibility(View.VISIBLE);
    }

    private void hideContentCategoryBuku(){
        findViewById(R.id.cat_buku).setVisibility(View.GONE);
    }

    private void dropdownSetup(){
        List<String> dataset = new LinkedList<>(Arrays.asList("Pilih Kategori","Koleksi Buku", "Koleksi Laporan"));
        final List<String> dataset_gmd = new LinkedList<>(Arrays.asList(
                "Pilih GMD",
                "Semua GMD / Media",
                "Text",
                "Art Original",
                "Chart",
                "Computer Software",
                "Diorama",
                "Filmstrip",
                "Flash Card",
                "Game",
                "Globe",
                "Kit",
                "Map",
                "Microform",
                "Manuscript",
                "Model",
                "Motion Picture",
                "Microscope Slide",
                "Multimedia",
                "Electronic Resource",
                "Digital Versatile Disc")
        );
        kategoriDropdown.attachDataSource(dataset);
        kategoriDropdown.animate();
        gmd.attachDataSource(dataset_gmd);
        gmd.animate();
        kategoriDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        hideContentCategoryBuku();
                        break;
                    case 1:
                        categoriesLocation = "Buku";
                        showContentCategoryBuku();
                        break;
                    case 2:
                        categoriesLocation = "Laporan";
                        hideContentCategoryBuku();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        gmd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0 || i == 1) {
                    gmd_val = "";
                } else {
                    isFullFilter = true;
                    String tmp_gmd = dataset_gmd.get(i).toLowerCase().toString();
                    gmd_val = tmp_gmd.replaceAll(" ","%20");
                    Toast.makeText(getApplicationContext(), gmd_val, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void sendResults(){
        parameter_configuration();
        Intent intent = new Intent();
        intent.putExtra("categoriesLocation", categoriesLocation);
        intent.putExtra("status", "from_filter");
        if (isFullFilter) {
            intent.putExtra("isFullFilter", "true");
            intent.putExtra("pengarang", pengarang_val);
            intent.putExtra("subyek", subyek_val);
            intent.putExtra("isbn", isbn_val);
            intent.putExtra("gmd", gmd_val);
        } else {
            intent.putExtra("isFullFilter", "false");
        }
        setResult(REQUEST_CODE,intent);
        finish();
    }

    private void parameter_configuration(){
        String tmp_subyek = subyek.getText().toString();
        String tmp_pengarang = pengarang.getText().toString();
        String tmp_isbn = isbn.getText().toString();
        if (tmp_subyek.isEmpty()){
            subyek_val = "";
        } else {
            subyek_val = tmp_subyek.replaceAll(" ","%20");
            isFullFilter = true;
        }

        if (tmp_pengarang.isEmpty()){
            pengarang_val = "";
        } else {
            pengarang_val = tmp_pengarang.replaceAll(" ","%20");
            isFullFilter = true;
        }

        if (tmp_isbn.isEmpty()){
            isbn_val = "";
        } else {
            isbn_val = tmp_isbn.replaceAll(" ","%20");
            isFullFilter = true;
        }
    }

    private void sendResultsIfBackPressed(){
        Intent intent = new Intent();
        intent.putExtra("categoriesLocation", "null");
        intent.putExtra("status", "back_pressed");
        setResult(REQUEST_CODE,intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            sendResultsIfBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        sendResultsIfBackPressed();
        super.onBackPressed();
    }
}
