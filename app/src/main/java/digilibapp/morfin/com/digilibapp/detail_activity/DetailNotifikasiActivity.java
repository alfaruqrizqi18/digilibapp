package digilibapp.morfin.com.digilibapp.detail_activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import digilibapp.morfin.com.digilibapp.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailNotifikasiActivity extends AppCompatActivity {
    String id_notifikasi, judul_notifikasi, pesan_notifikasi, tgl_notifikasi;
    TextView tv_judul_notifikasi, tv_pesan_notifikasi, tv_tgl_notifikasi;
    FrameLayout card_edge;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_notifikasi);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Pemberitahuan");
        id_notifikasi = getIntent().getStringExtra("id_notifikasi");
        judul_notifikasi = getIntent().getStringExtra("judul_notifikasi");
        pesan_notifikasi = getIntent().getStringExtra("pesan_notifikasi");
        tgl_notifikasi = getIntent().getStringExtra("tgl_notifikasi");

        tv_judul_notifikasi = (TextView) findViewById(R.id.judul_notifikasi);
        tv_pesan_notifikasi = (TextView) findViewById(R.id.pesan_notifikasi);
        tv_tgl_notifikasi = (TextView) findViewById(R.id.tgl_notifikasi);
        card_edge = findViewById(R.id.card_edge);


        starterPack();

    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void starterPack(){
        if (Integer.parseInt(id_notifikasi)%2 == 0 ) {
            card_edge.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        } else {
            card_edge.setBackgroundColor(getResources().getColor(R.color.green));
        }
        tv_judul_notifikasi.setText(judul_notifikasi);
        tv_pesan_notifikasi.setText(pesan_notifikasi);
        tv_tgl_notifikasi.setText("diterima pada "+tgl_notifikasi);
    }
}
