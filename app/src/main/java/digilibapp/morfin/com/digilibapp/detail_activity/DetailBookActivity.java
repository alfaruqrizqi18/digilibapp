package digilibapp.morfin.com.digilibapp.detail_activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.sharedpreference.SharedPrefManager;
import digilibapp.morfin.com.digilibapp.url.URLs;
import digilibapp.morfin.com.digilibapp.volley.VolleySingleton;
import mehdi.sakout.fancybuttons.FancyButton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailBookActivity extends AppCompatActivity {
    String get_thumbnail_url, get_judul_value, get_stock_value, get_jurusan_value;
    boolean is_available;
    String id_buku, judul_buku;
    FancyButton buttonPesan;
    ImageView thumbnail;
    TextView title, jurusan, stock;
    TextView pengarang, edisi, no_panggil, isbn, subyek, gmd, bahasa, penerbit, tahun_terbit, tempat_terbit, deskripsi, letak_buku;
    SweetAlertDialog sweetAlertDialog;
    ProgressBar progressBar;
    LinearLayout upContainer, buttonContainer, layout_info_buku;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_book);
        id_buku = getIntent().getStringExtra("id_buku").toString();
        judul_buku = getIntent().getStringExtra("judul_buku").toString();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setSubtitle(judul_buku);
        sweetAlertDialog = new SweetAlertDialog(DetailBookActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        upContainer = (LinearLayout)findViewById(R.id.upContainer);
        buttonContainer = (LinearLayout)findViewById(R.id.buttonContainer);
        layout_info_buku = (LinearLayout)findViewById(R.id.layout_info_buku);
        //=============================================

        thumbnail = (ImageView)findViewById(R.id.thumbnail);
        title = (TextView)findViewById(R.id.title);
        jurusan = (TextView)findViewById(R.id.jurusan);
        stock = (TextView)findViewById(R.id.stock);
        buttonPesan = (FancyButton)findViewById(R.id.buttonPesan);


        pengarang = (TextView)findViewById(R.id.pengarang);
        edisi = (TextView)findViewById(R.id.edisi);
        no_panggil = (TextView)findViewById(R.id.no_panggil);
        isbn = (TextView)findViewById(R.id.isbn);
        subyek = (TextView)findViewById(R.id.subyek);
        gmd = (TextView)findViewById(R.id.gmd);
        bahasa = (TextView)findViewById(R.id.bahasa);
        penerbit = (TextView)findViewById(R.id.penerbit);
        tahun_terbit = (TextView)findViewById(R.id.tahun_terbit);
        tempat_terbit = (TextView)findViewById(R.id.tempat_terbit);
        deskripsi = (TextView)findViewById(R.id.deskripsi);
        letak_buku = (TextView)findViewById(R.id.letak_buku);
        //=============================================

        starterPack();

    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void starterPack(){
        showDetailBook();
        buttonPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBooking();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void showResult(){
        progressBar.setVisibility(View.GONE);
        upContainer.setVisibility(View.VISIBLE);
        buttonContainer.setVisibility(View.VISIBLE);
        layout_info_buku.setVisibility(View.VISIBLE);
    }

    private void beforeLoad(){
        progressBar.setVisibility(View.VISIBLE);
        upContainer.setVisibility(View.GONE);
        buttonContainer.setVisibility(View.GONE);
        layout_info_buku.setVisibility(View.GONE);
    }

    public void showDetailBook(){
        beforeLoad();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.get_stock_and_detail_book+id_buku,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showResult();
                        try {
                            JSONObject responses = new JSONObject(response);
                            get_stock_value = responses.getString("total_stock");
                            JSONArray results = responses.getJSONArray("results");

                            if (get_stock_value.equals("0")) {
                                get_stock_value = "Saat ini buku tidak tersedia";
                                buttonPesan.setBackgroundColor(getResources().getColor(R.color.red));
                                buttonPesan.setFocusBackgroundColor(getResources().getColor(R.color.dark_red));
                                buttonPesan.setText("TIDAK TERSEDIA");
                                is_available = false;
                            } else {
                                get_stock_value = "Buku ini tersedia " + responses.getString("total_stock") + " buah";
                                buttonPesan.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                is_available = true;
                            }

                            for (int i = 0; i < results.length(); i++) {
                                JSONObject info_data = results.getJSONObject(i);
                                get_thumbnail_url = info_data.getString("thumbnail");
                                get_judul_value = info_data.getString("judul_buku");
                                get_jurusan_value = info_data.getString("jurusan");
                                Glide.with(getApplicationContext()).load(URLs.base_url_web.toString() + get_thumbnail_url).into(thumbnail);
                                title.setText(get_judul_value);
                                stock.setText(get_stock_value);
                                jurusan.setText(get_jurusan_value);
                                pengarang.setText(info_data.getString("pengarang"));
                                edisi.setText(info_data.getString("edisi"));
                                no_panggil.setText(info_data.getString("no_panggil"));
                                isbn.setText(info_data.getString("isbn"));
                                subyek.setText(info_data.getString("subyek"));
                                gmd.setText(info_data.getString("gmd"));
                                bahasa.setText(info_data.getString("bahasa"));
                                penerbit.setText(info_data.getString("penerbit"));
                                tahun_terbit.setText(info_data.getString("tahun_terbit"));
                                tempat_terbit.setText(info_data.getString("tempat_terbit"));
                                deskripsi.setText(info_data.getString("deskripsi_fisik"));
                                letak_buku.setText(info_data.getString("letak_buku"));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    jurusan.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_icon_trophy, 0, 0, 0);
                                    jurusan.setCompoundDrawablePadding(10);
                                    stock.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_icon_clip, 0, 0, 0);
                                    stock.setCompoundDrawablePadding(10);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void onBooking(){
        if (is_available) {
            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setContentText("Apa kamu yakin ?")
                    .setConfirmText("Ya")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            onBookingProccess();
                            sDialog.dismissWithAnimation();
                        }
                    })
                    .setCancelButton("Tidak", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    })
                    .show();
        } else {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setContentText("Maaf buku ini tidak tersedia untuk saat ini")
                    .show();
        }
    }

    public void onBookingProccess(){
        final String bookId = id_buku;
        final String nim = SharedPrefManager.getInstance(DetailBookActivity.this).showSession().get("NIM").toString();
        sweetAlertDialog.getProgressHelper().setBarColor(R.color.colorPrimary);
        sweetAlertDialog.setTitleText("Mohon tunggu sebentar...");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.store_transaction_proccess,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            sweetAlertDialog.dismiss();
                            JSONObject obj = new JSONObject(response);
                            //getting the user from the response
                            JSONArray results = obj.getJSONArray("results");
                            String status = obj.getString("status").toString();
                            if (status.equals("OK")) {
                                JSONObject data = results.getJSONObject(0);
                                String nomor_pemesanan_unik = data.getString("nomor_pemesanan_unik");
                                String id_pemesanan = data.getString("id_pemesanan");
                                showSuccessResultsDialog(nomor_pemesanan_unik, id_pemesanan);
                            } else if(status.equals("NULL")){
                                showNullOrDuplicateResultsDialog("Maaf buku tersebut belum tersedia lagi");
                            } else if(status.equals("DUPLICATE")){
                                showNullOrDuplicateResultsDialog("Kamu sudah memesan buku ini.");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.printf("Error : " + e);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("nim", nim);
                params.put("id_buku", bookId);
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void showNullOrDuplicateResultsDialog(String message) {
        new SweetAlertDialog(DetailBookActivity.this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(message)
                .setConfirmText("Okay")
                .show();
    }

    public void showSuccessResultsDialog(final String nomor_pemesanan_unik, final String id_pemesanan){
        new SweetAlertDialog(DetailBookActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                .setContentText("Buku berhasil dipesan")
                .setConfirmText("Lihat Detail")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        finish();
                        Intent intent = new Intent(DetailBookActivity.this, DetailTransaksiActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("nomor_pemesanan_unik", nomor_pemesanan_unik);
                        intent.putExtra("id_pemesanan", id_pemesanan);
                        startActivity(intent);
                        sDialog.dismissWithAnimation();
                    }
                })
                .setCancelButton("Tutup", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        finish();
                        sDialog.dismissWithAnimation();
                    }
                }).show();
    }



}
