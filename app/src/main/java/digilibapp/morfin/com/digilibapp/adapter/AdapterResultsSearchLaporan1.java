package digilibapp.morfin.com.digilibapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.rxdownloader.RxDownloader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.detail_activity.DetailLaporanActivity;
import digilibapp.morfin.com.digilibapp.models.Laporan;
import digilibapp.morfin.com.digilibapp.url.URLs;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Morfin on 1/25/2018.
 */

public class AdapterResultsSearchLaporan1 extends RecyclerView.Adapter<AdapterResultsSearchLaporan1.LaporanResults1ViewHolder> {
    private Context mContext;
    private List<Laporan> laporanList;

    public AdapterResultsSearchLaporan1(Context mContext, List<Laporan> laporanList) {
        this.mContext = mContext;
        this.laporanList = laporanList;
    }

    public class LaporanResults1ViewHolder extends RecyclerView.ViewHolder{
        public ImageView thumbnail_laporan;
        public TextView judul_laporan, jurusan;
        CardView card_view_laporan;
        public LaporanResults1ViewHolder(View itemView) {
            super(itemView);
            thumbnail_laporan = (ImageView)itemView.findViewById(R.id.thumbnail_laporan);
            judul_laporan = (TextView)itemView.findViewById(R.id.judul_laporan);
            jurusan = (TextView)itemView.findViewById(R.id.jurusan);
            card_view_laporan = (CardView)itemView.findViewById(R.id.card_view_laporan);
        }
    }
    @Override
    public AdapterResultsSearchLaporan1.LaporanResults1ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_results1_laporan_item, parent, false);

        return new LaporanResults1ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AdapterResultsSearchLaporan1.LaporanResults1ViewHolder holder, int position) {
        final Laporan laporan = laporanList.get(position);
        holder.judul_laporan.setText(laporan.getJudul_laporan());
        holder.jurusan.setText(laporan.getJurusan());
        Glide.with(mContext).load(URLs.base_url_web.toString() + laporan.getThumbnail()).into(holder.thumbnail_laporan);
        holder.thumbnail_laporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail_laporan(
                        laporan.getId_laporan(), laporan.getJudul_laporan(), laporan.getTahun(), laporan.getAbstrak(),
                        laporan.getKata_kunci(), laporan.getJenis_laporan(), URLs.base_url_web.toString() + laporan.getAlamat_file_laporan(), laporan.getJurusan(),
                        laporan.getThumbnail());
            }
        });
        holder.judul_laporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail_laporan(
                        laporan.getId_laporan(), laporan.getJudul_laporan(), laporan.getTahun(), laporan.getAbstrak(),
                        laporan.getKata_kunci(), laporan.getJenis_laporan(), URLs.base_url_web.toString() + laporan.getAlamat_file_laporan(), laporan.getJurusan(),
                        laporan.getThumbnail());
            }
        });

        holder.jurusan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail_laporan(
                        laporan.getId_laporan(), laporan.getJudul_laporan(), laporan.getTahun(), laporan.getAbstrak(),
                        laporan.getKata_kunci(), laporan.getJenis_laporan(), URLs.base_url_web.toString() + laporan.getAlamat_file_laporan(), laporan.getJurusan(),
                        laporan.getThumbnail());
            }
        });

        holder.card_view_laporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detail_laporan(
                        laporan.getId_laporan(), laporan.getJudul_laporan(), laporan.getTahun(), laporan.getAbstrak(),
                        laporan.getKata_kunci(), laporan.getJenis_laporan(), URLs.base_url_web.toString() + laporan.getAlamat_file_laporan(), laporan.getJurusan(),
                        laporan.getThumbnail());
            }
        });
    }

    private void choice_dialog(final String url, final String judul_laporan){
        new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE)
                .setContentText("Ingin mengunduh ?")
                .setConfirmButton("Ya, unduh", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                            download_files(url, judul_laporan + ".docx", "application/msword");
                        } else if (url.toString().contains(".pdf")){
                            download_files(url, judul_laporan + ".pdf","application/pdf");
                        }
                        sDialog.dismissWithAnimation();
                    }
                }).show();
    }

    private void detail_laporan(String id_laporan, String judul_laporan, String tahun, String abstrak,
                                String kata_kunci, String jenis_laporan, String alamat_file_laporan,
                                String jurusan, String thumbnail){

        Intent intent = new Intent(mContext, DetailLaporanActivity.class);
        intent.putExtra("id_laporan", id_laporan);
        intent.putExtra("judul_laporan", judul_laporan);
        intent.putExtra("tahun", tahun);
        intent.putExtra("abstrak", abstrak);
        intent.putExtra("kata_kunci", kata_kunci);
        intent.putExtra("jenis_laporan", jenis_laporan);
        intent.putExtra("alamat_file_laporan", alamat_file_laporan);
        intent.putExtra("jurusan", jurusan);
        intent.putExtra("thumbnail", thumbnail);
        mContext.startActivity(intent);
    }

    private void download_files(final String url, String filename, final String mimeType){
        RxDownloader rxDownloader = new RxDownloader(mContext);
        rxDownloader.download(url, filename, mimeType, true)
        .subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String filePath) {
                try {
                    URL urls = new URL(filePath.replaceAll("%20", " "));
                    Toast.makeText(mContext, "Lokasi file : "+ filePath, Toast.LENGTH_LONG).show();
                    File temp_file = new File(urls.getFile());
                    Intent intent = new Intent("android.intent.action.VIEW");

                    if (mimeType.equals("application/msword")) {
                        intent.setDataAndType(Uri.fromFile(temp_file),"application/msword");
                        mContext.startActivity(intent);
                    } else {
                        intent.setDataAndType(Uri.fromFile(temp_file),"application/pdf");
                        mContext.startActivity(intent);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
            }
        });
    }

    @Override
    public int getItemCount() {
        return laporanList.size();
    }


}
