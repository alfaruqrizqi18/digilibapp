package digilibapp.morfin.com.digilibapp.models;

/**
 * Created by Morfin on 2/25/2018.
 */

public class TransaksiPeminjaman {
    String id_transaksi, tgl_transaksi, tgl_pinjam_buku, tgl_kembali_buku, tgl_buku_dikembalikan, status_transaksi, total_denda,
            nama, judul_buku, thumbnail;

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getTgl_transaksi() {
        return tgl_transaksi;
    }

    public void setTgl_transaksi(String tgl_transaksi) {
        this.tgl_transaksi = tgl_transaksi;
    }

    public String getTgl_pinjam_buku() {
        return tgl_pinjam_buku;
    }

    public void setTgl_pinjam_buku(String tgl_pinjam_buku) {
        this.tgl_pinjam_buku = tgl_pinjam_buku;
    }

    public String getTgl_kembali_buku() {
        return tgl_kembali_buku;
    }

    public void setTgl_kembali_buku(String tgl_kembali_buku) {
        this.tgl_kembali_buku = tgl_kembali_buku;
    }

    public String getTgl_buku_dikembalikan() {
        return tgl_buku_dikembalikan;
    }

    public void setTgl_buku_dikembalikan(String tgl_buku_dikembalikan) {
        this.tgl_buku_dikembalikan = tgl_buku_dikembalikan;
    }

    public String getStatus_transaksi() {
        return status_transaksi;
    }

    public void setStatus_transaksi(String status_transaksi) {
        this.status_transaksi = status_transaksi;
    }

    public String getTotal_denda() {
        return total_denda;
    }

    public void setTotal_denda(String total_denda) {
        this.total_denda = total_denda;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJudul_buku() {
        return judul_buku;
    }

    public void setJudul_buku(String judul_buku) {
        this.judul_buku = judul_buku;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

}
