package digilibapp.morfin.com.digilibapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.detail_activity.DetailTransaksiPeminjamanActivity;
import digilibapp.morfin.com.digilibapp.models.TransaksiPeminjaman;
import digilibapp.morfin.com.digilibapp.url.URLs;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Morfin on 2/25/2018.
 */

public class AdapterAllTransactionPeminjaman extends RecyclerView.Adapter<AdapterAllTransactionPeminjaman.TransactionResultsViewHolder> {
    private Context mContext;
    private List<TransaksiPeminjaman> transaksiPeminjamanList;

    public AdapterAllTransactionPeminjaman(Context mContext, List<TransaksiPeminjaman> transaksiPeminjamanList) {
        this.mContext = mContext;
        this.transaksiPeminjamanList = transaksiPeminjamanList;
    }


    public class TransactionResultsViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail_buku;
        TextView judul_buku, id_transaksi;
        CardView card_view;
        FancyButton status_transaksi;

        public TransactionResultsViewHolder(View itemView) {
            super(itemView);
            thumbnail_buku = (ImageView)itemView.findViewById(R.id.thumbnail_buku);
            judul_buku = (TextView)itemView.findViewById(R.id.judul_buku);
            id_transaksi = (TextView)itemView.findViewById(R.id.id_transaksi);
            card_view = (CardView)itemView.findViewById(R.id.card_view);
            status_transaksi = (FancyButton) itemView.findViewById(R.id.status_transaksi);
        }
    }

    @Override
    public TransactionResultsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_transaction_peminjaman_item, parent, false);

        return new TransactionResultsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TransactionResultsViewHolder holder, int position) {
        final TransaksiPeminjaman transaksiPeminjaman = transaksiPeminjamanList.get(position);
        String get_status_transaksi = transaksiPeminjaman.getStatus_transaksi();
        holder.judul_buku.setText(transaksiPeminjaman.getJudul_buku());
        holder.id_transaksi.setText(transaksiPeminjaman.getId_transaksi());
        Glide.with(mContext)
                .load(URLs.base_url_web.toString() + transaksiPeminjaman.getThumbnail())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.thumbnail_buku);
        if (get_status_transaksi.equals("peminjaman_sedang_berlangsung")) {
            holder.status_transaksi.setText("Berlangsung");
            holder.status_transaksi.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            holder.status_transaksi.setFocusBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        } else if (get_status_transaksi.equals("peminjaman_melewati_batas")) {
            holder.status_transaksi.setText("Lewat Batas");
            holder.status_transaksi.setBackgroundColor(mContext.getResources().getColor(R.color.red));
            holder.status_transaksi.setFocusBackgroundColor(mContext.getResources().getColor(R.color.dark_red));
        } else if (get_status_transaksi.equals("peminjaman_telah_berakhir")) {
            holder.status_transaksi.setText("Berakhir");
            holder.status_transaksi.setBackgroundColor(mContext.getResources().getColor(R.color.green));
            holder.status_transaksi.setFocusBackgroundColor(mContext.getResources().getColor(R.color.dark_green));
        }

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDetailTransaction(transaksiPeminjaman.getId_transaksi());
            }
        });

        holder.thumbnail_buku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDetailTransaction(transaksiPeminjaman.getId_transaksi());
            }
        });

        holder.judul_buku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDetailTransaction(transaksiPeminjaman.getId_transaksi());
            }
        });


    }

    public void openDetailTransaction(String id_transaksi){
        Intent intent = new Intent(mContext, DetailTransaksiPeminjamanActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id_transaksi", id_transaksi);
        mContext.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return transaksiPeminjamanList.size();
    }

}
