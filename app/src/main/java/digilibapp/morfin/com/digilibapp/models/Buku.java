package digilibapp.morfin.com.digilibapp.models;

/**
 * Created by Morfin on 1/25/2018.
 */

public class Buku {
    private String id_buku, judul_buku, pengarang, edisi, jurusan, no_panggil, isbn,
            subyek, gmd, bahasa, penerbit, tahun_terbit, tempat_terbit, deskripsi_fisik, letak_buku, thumbnail;

    public void setId_buku(String id_buku) {
        this.id_buku = id_buku;
    }

    public void setJudul_buku(String judul_buku) {
        this.judul_buku = judul_buku;
    }

    public void setPengarang(String pengarang) {
        this.pengarang = pengarang;
    }

    public void setEdisi(String edisi) {
        this.edisi = edisi;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public void setNo_panggil(String no_panggil) {
        this.no_panggil = no_panggil;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setSubyek(String subyek) {
        this.subyek = subyek;
    }

    public void setGmd(String gmd) {
        this.gmd = gmd;
    }

    public void setBahasa(String bahasa) {
        this.bahasa = bahasa;
    }

    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    public void setTahun_terbit(String tahun_terbit) {
        this.tahun_terbit = tahun_terbit;
    }

    public void setTempat_terbit(String tempat_terbit) {
        this.tempat_terbit = tempat_terbit;
    }

    public void setDeskripsi_fisik(String deskripsi_fisik) {
        this.deskripsi_fisik = deskripsi_fisik;
    }

    public void setLetak_buku(String letak_buku) {
        this.letak_buku = letak_buku;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getId_buku() {
        return id_buku;
    }

    public String getJudul_buku() {
        return judul_buku;
    }

    public String getPengarang() {
        return pengarang;
    }

    public String getEdisi() {
        return edisi;
    }

    public String getJurusan() {
        return jurusan;
    }

    public String getNo_panggil() {
        return no_panggil;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getSubyek() {
        return subyek;
    }

    public String getGmd() {
        return gmd;
    }

    public String getBahasa() {
        return bahasa;
    }

    public String getPenerbit() {
        return penerbit;
    }

    public String getTahun_terbit() {
        return tahun_terbit;
    }

    public String getTempat_terbit() {
        return tempat_terbit;
    }

    public String getDeskripsi_fisik() {
        return deskripsi_fisik;
    }

    public String getLetak_buku() {
        return letak_buku;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}
