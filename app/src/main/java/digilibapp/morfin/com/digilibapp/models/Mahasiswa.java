package digilibapp.morfin.com.digilibapp.models;

/**
 * Created by Morfin on 1/25/2018.
 */

public class Mahasiswa {
    public String id_mahasiswa, nim, password, nama, jurusan, token_android;
    public Mahasiswa(String id_mahasiswa, String nim, String password, String nama, String jurusan, String token_android) {
        this.id_mahasiswa = id_mahasiswa;
        this.nim = nim;
        this.password = password;
        this.nama = nama;
        this.jurusan = jurusan;
        this.token_android = token_android;
    }
    public String getId_mahasiswa() {
        return id_mahasiswa;
    }

    public void setId_mahasiswa(String id_mahasiswa) {
        this.id_mahasiswa = id_mahasiswa;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getToken_android() {
        return token_android;
    }

    public void setToken_android(String token_android) {
        this.token_android = token_android;
    }
}
