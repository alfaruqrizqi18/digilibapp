package digilibapp.morfin.com.digilibapp.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import digilibapp.morfin.com.digilibapp.FilterActivity;
import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.adapter.AdapterResultsSearchLaporan1;
import digilibapp.morfin.com.digilibapp.adapter.AdapterResultsSearchBooks1;
import digilibapp.morfin.com.digilibapp.models.Buku;
import digilibapp.morfin.com.digilibapp.models.Laporan;
import digilibapp.morfin.com.digilibapp.url.URLs;
import digilibapp.morfin.com.digilibapp.volley.VolleySingleton;

/**
 * Created by Morfin on 1/24/2018.
 */

public class FragmentBuku extends Fragment {
    int REQUEST_CODE = 1;
    String queries = "QUERIES_IS_EMPTY";
    String categoriesLocation = "Buku";
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    RecyclerView recyclerViewContainer;
    SwipeRefreshLayout swipeRefreshLayout;
    SweetAlertDialog sweetAlertDialog;
    ProgressBar progressBar;
    View oops;

    AdapterResultsSearchBooks1 adapterResultsSearchBooks1;
    AdapterResultsSearchLaporan1 adapterResultsLaporan1;
    public FragmentBuku() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buku, container, false);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayout);
        sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        recyclerViewContainer = (RecyclerView)view.findViewById(R.id.recyclerViewContainer);
        oops = view.findViewById(R.id.oops);
        starterPack();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.pencarian_option, menu);
        MenuItem searchIem = menu.findItem(R.id.search);
        MenuItem filterItem = menu.findItem(R.id.action_filter);

        final SearchView searchView = (SearchView) searchIem.getActionView();
        searchView.setQueryHint("Saya ingin mencari...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public boolean onQueryTextSubmit(String query) {
                queries = query;
                if (categoriesLocation.equals("Buku")){
                    searchBuku(query);
                } else {
                    searchLaporan(query);
                }
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        filterItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                showFilterDialog();
                return true;
            }
        });
    }

    private void starterPack(){
        landingPageBooks();
        oops.setVisibility(View.GONE);
        recyclerViewContainer.setNestedScrollingEnabled(false);
        recyclerViewContainer.setOnScrollListener(new RecyclerView.OnScrollListener() {
            private boolean scrollEnabled;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ?
                                0 : recyclerView.getChildAt(0).getTop();

                boolean newScrollEnabled =
                        (dx == 0 && topRowVerticalPosition >= 0) ?
                                true : false;

                if (null != swipeRefreshLayout && scrollEnabled != newScrollEnabled) {
                    // Start refreshing....
                    swipeRefreshLayout.setEnabled(newScrollEnabled);
                    scrollEnabled = newScrollEnabled;
                }
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (categoriesLocation.equals("Buku")) {
                    landingPageBooks();
                } else if (categoriesLocation.equals("Laporan")){
                    landingPageLaporan();
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recyclerViewContainer.setAdapter(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            String pengarang, subyek, isbn, gmd;
            categoriesLocation = data.getStringExtra("categoriesLocation");
            String isFullFilter = data.getStringExtra("isFullFilter");
            String status = data.getStringExtra("status");

            if (status.equals("back_pressed")) {

            } else {
                if (categoriesLocation.equals("Buku")) {
                    if (isFullFilter.equals("true")) {
                        pengarang = data.getStringExtra("pengarang");
                        subyek = data.getStringExtra("subyek");
                        isbn = data.getStringExtra("isbn");
                        gmd = data.getStringExtra("gmd");

                        if (queries.equals("QUERIES_IS_EMPTY")) {
                            landingPageBooksWithFullFilter(pengarang, subyek, isbn, gmd);
                        } else {
                            searchBukuWithFullFilter(queries, pengarang, subyek, isbn, gmd);
                        }
                    } else {
                        if (categoriesLocation.equals("Buku")){
                            if (queries.equals("QUERIES_IS_EMPTY")) {
                                landingPageBooks();
                            } else {
                                searchBuku(queries);
                            }
                        }
                    }
                } else if(categoriesLocation.equals("Laporan")){
                    if (categoriesLocation.equals("Laporan")) {
                        if (queries.equals("QUERIES_IS_EMPTY")) {
                            landingPageLaporan();
                        } else {
                            searchLaporan(queries);
                        }
                    }
                }
            }
        }
    }

    private void showFilterDialog(){
        Intent intent = new Intent(getContext(), FilterActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    public void searchBuku(String param){
        progressBar.setVisibility(View.VISIBLE);
        oops.setVisibility(View.GONE);
        final String parameter = param.toString().trim().replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.search_url_books_1+parameter,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<Buku> dataBukuResults1 = new ArrayList<>();
                        try {
                            dataBukuResults1.clear();
                            recyclerViewContainer.setLayoutManager(null);
                            String id_buku, judul_buku, thumbnail_buku, jurusan;
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            if (results.length() < 1) {
                                oops.setVisibility(View.VISIBLE);
                            } else {
                                oops.setVisibility(View.GONE);
                                for (int i = 0; i < results.length(); i++) {
                                    JSONObject info_data = results.getJSONObject(i);
                                    Buku buku = new Buku();
                                    id_buku = info_data.getString("id_buku");
                                    judul_buku = info_data.getString("judul_buku");
                                    thumbnail_buku = info_data.getString("thumbnail");
                                    jurusan = info_data.getString("jurusan");
                                    buku.setId_buku(id_buku);
                                    buku.setJudul_buku(judul_buku);
                                    buku.setThumbnail(thumbnail_buku);
                                    buku.setJurusan(jurusan);
                                    dataBukuResults1.add(buku);
                                }
                            }
                            adapterResultsSearchBooks1 = new AdapterResultsSearchBooks1(getContext(), dataBukuResults1);
                            recyclerViewContainer.setLayoutManager(staggeredGridLayoutManager);
                            recyclerViewContainer.setAdapter(adapterResultsSearchBooks1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    public void searchLaporan(String param){
        progressBar.setVisibility(View.VISIBLE);
        oops.setVisibility(View.GONE);
        final String parameter = param.toString().trim().replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.search_url_laporan_1 + parameter,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<Laporan> dataLaporanResults1 = new ArrayList<>();
                        try {
                            dataLaporanResults1.clear();
                            recyclerViewContainer.setLayoutManager(null);
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            if (results.length() < 1) {
                                oops.setVisibility(View.VISIBLE);
                            } else {
                                oops.setVisibility(View.GONE);
                                for (int i = 0; i < results.length(); i++) {
                                    JSONObject info_data = results.getJSONObject(i);
                                    Laporan laporan = new Laporan();

                                    laporan.setId_laporan(info_data.getString("id_laporan"));
                                    laporan.setJudul_laporan(info_data.getString("judul_laporan"));
                                    laporan.setTahun(info_data.getString("tahun"));
                                    laporan.setThumbnail(info_data.getString("thumbnail"));
                                    laporan.setKata_kunci(info_data.getString("kata_kunci"));
                                    laporan.setJurusan(info_data.getString("jurusan"));
                                    laporan.setAlamat_file_laporan(info_data.getString("alamat_file_laporan_public"));
                                    laporan.setAbstrak(info_data.getString("abstrak"));
                                    laporan.setJenis_laporan(info_data.getString("jenis_laporan"));

                                    dataLaporanResults1.add(laporan);
                                }
                            }
                            adapterResultsLaporan1 = new AdapterResultsSearchLaporan1(getContext(), dataLaporanResults1);
                            recyclerViewContainer.setLayoutManager(staggeredGridLayoutManager);
                            recyclerViewContainer.setAdapter(adapterResultsLaporan1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    public void landingPageBooks(){
        progressBar.setVisibility(View.VISIBLE);
        oops.setVisibility(View.GONE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.landingpage_books,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<digilibapp.morfin.com.digilibapp.models.Buku> dataBukuResults1 = new ArrayList<>();
                        try {
                            dataBukuResults1.clear();
                            recyclerViewContainer.setLayoutManager(null);
                            String id_buku, judul_buku, thumbnail_buku, jurusan;
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            if (results.length() < 1) {
                                oops.setVisibility(View.VISIBLE);
                            } else {
                                oops.setVisibility(View.GONE);
                                for (int i = 0; i < results.length(); i++) {
                                    JSONObject info_data = results.getJSONObject(i);
                                    Buku buku = new Buku();
                                    id_buku = info_data.getString("id_buku");
                                    judul_buku = info_data.getString("judul_buku");
                                    thumbnail_buku = info_data.getString("thumbnail");
                                    jurusan = info_data.getString("jurusan");
                                    buku.setId_buku(id_buku);
                                    buku.setJudul_buku(judul_buku);
                                    buku.setThumbnail(thumbnail_buku);
                                    buku.setJurusan(jurusan);
                                    dataBukuResults1.add(buku);
                                }
                            }
                            adapterResultsSearchBooks1 = new AdapterResultsSearchBooks1(getContext(), dataBukuResults1);
                            recyclerViewContainer.setLayoutManager(staggeredGridLayoutManager);
                            recyclerViewContainer.setAdapter(adapterResultsSearchBooks1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    public void landingPageLaporan(){
        progressBar.setVisibility(View.VISIBLE);
        oops.setVisibility(View.GONE);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.landingpage_laporan,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<Laporan> dataLaporanResults1 = new ArrayList<>();
                        try {
                            dataLaporanResults1.clear();
                            recyclerViewContainer.setLayoutManager(null);
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            if (results.length() < 1) {
                                oops.setVisibility(View.VISIBLE);
                            } else {
                                oops.setVisibility(View.GONE);
                                for (int i = 0; i < results.length(); i++) {
                                    JSONObject info_data = results.getJSONObject(i);
                                    Laporan laporan = new Laporan();
                                    laporan.setId_laporan(info_data.getString("id_laporan"));
                                    laporan.setJudul_laporan(info_data.getString("judul_laporan"));
                                    laporan.setTahun(info_data.getString("tahun"));
                                    laporan.setThumbnail(info_data.getString("thumbnail"));
                                    laporan.setKata_kunci(info_data.getString("kata_kunci"));
                                    laporan.setJurusan(info_data.getString("jurusan"));
                                    laporan.setAlamat_file_laporan(info_data.getString("alamat_file_laporan_public"));
                                    laporan.setAbstrak(info_data.getString("abstrak"));
                                    laporan.setJenis_laporan(info_data.getString("jenis_laporan"));
                                    dataLaporanResults1.add(laporan);
                                }
                            }
                            adapterResultsLaporan1 = new AdapterResultsSearchLaporan1(getContext(), dataLaporanResults1);
                            recyclerViewContainer.setLayoutManager(staggeredGridLayoutManager);
                            recyclerViewContainer.setAdapter(adapterResultsLaporan1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    public void searchBukuWithFullFilter(String judul, String pengarang, String subyek, String isbn, String gmd){
        progressBar.setVisibility(View.VISIBLE);
        oops.setVisibility(View.GONE);
        String additional_url = "judul=" + judul + "&pengarang=" + pengarang + "&subyek=" + subyek + "&isbn=" + isbn + "&gmd=" + gmd;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.search_url_books_1_full_filter + additional_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<Buku> dataBukuResults1 = new ArrayList<>();
                        try {
                            dataBukuResults1.clear();
                            recyclerViewContainer.setLayoutManager(null);
                            String id_buku, judul_buku, thumbnail_buku, jurusan;
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            if (results.length() < 1) {
                                oops.setVisibility(View.VISIBLE);
                            } else {
                                oops.setVisibility(View.GONE);
                                for (int i = 0; i < results.length(); i++) {
                                    JSONObject info_data = results.getJSONObject(i);
                                    Buku buku = new Buku();
                                    id_buku = info_data.getString("id_buku");
                                    judul_buku = info_data.getString("judul_buku");
                                    thumbnail_buku = info_data.getString("thumbnail");
                                    jurusan = info_data.getString("jurusan");
                                    buku.setId_buku(id_buku);
                                    buku.setJudul_buku(judul_buku);
                                    buku.setThumbnail(thumbnail_buku);
                                    buku.setJurusan(jurusan);
                                    dataBukuResults1.add(buku);
                                }
                            }
                            adapterResultsSearchBooks1 = new AdapterResultsSearchBooks1(getContext(), dataBukuResults1);
                            recyclerViewContainer.setLayoutManager(staggeredGridLayoutManager);
                            recyclerViewContainer.setAdapter(adapterResultsSearchBooks1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    public void landingPageBooksWithFullFilter(String pengarang, String subyek, String isbn, String gmd){
        progressBar.setVisibility(View.VISIBLE);
        oops.setVisibility(View.GONE);
        String additional_url = "pengarang=" + pengarang + "&subyek=" + subyek + "&isbn=" + isbn + "&gmd=" + gmd;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.landingpage_books_full_filter + additional_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<digilibapp.morfin.com.digilibapp.models.Buku> dataBukuResults1 = new ArrayList<>();
                        try {
                            dataBukuResults1.clear();
                            recyclerViewContainer.setLayoutManager(null);
                            String id_buku, judul_buku, thumbnail_buku, jurusan;
                            JSONObject responses = new JSONObject(response);
                            Log.d("Data from server", response);
                            JSONArray results = responses.getJSONArray("results");
                            if (results.length() < 1) {
                                oops.setVisibility(View.VISIBLE);
                            } else {
                                oops.setVisibility(View.GONE);
                                for (int i = 0; i < results.length(); i++) {
                                    JSONObject info_data = results.getJSONObject(i);
                                    Buku buku = new Buku();
                                    id_buku = info_data.getString("id_buku");
                                    judul_buku = info_data.getString("judul_buku");
                                    thumbnail_buku = info_data.getString("thumbnail");
                                    jurusan = info_data.getString("jurusan");
                                    buku.setId_buku(id_buku);
                                    buku.setJudul_buku(judul_buku);
                                    buku.setThumbnail(thumbnail_buku);
                                    buku.setJurusan(jurusan);
                                    dataBukuResults1.add(buku);
                                }
                            }
                            adapterResultsSearchBooks1 = new AdapterResultsSearchBooks1(getContext(), dataBukuResults1);
                            recyclerViewContainer.setLayoutManager(staggeredGridLayoutManager);
                            recyclerViewContainer.setAdapter(adapterResultsSearchBooks1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }
}
