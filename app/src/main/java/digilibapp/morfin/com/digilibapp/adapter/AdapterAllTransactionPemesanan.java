package digilibapp.morfin.com.digilibapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.detail_activity.DetailTransaksiActivity;
import digilibapp.morfin.com.digilibapp.models.Transaksi;
import digilibapp.morfin.com.digilibapp.url.URLs;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Morfin on 2/10/2018.
 */

public class AdapterAllTransactionPemesanan extends RecyclerView.Adapter<AdapterAllTransactionPemesanan.TransactionResultsViewHolder> {
    private Context mContext;
    private List<Transaksi> transaksiList;

    public AdapterAllTransactionPemesanan(Context mContext, List<Transaksi> transaksiList) {
        this.mContext = mContext;
        this.transaksiList = transaksiList;
    }

    public class TransactionResultsViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail_buku;
        TextView judul_buku, nomor_pemesanan_unik;
        CardView card_view;
        FancyButton status_booking;
        String status;
        public TransactionResultsViewHolder(View itemView) {
            super(itemView);
            thumbnail_buku = (ImageView)itemView.findViewById(R.id.thumbnail_buku);
            judul_buku = (TextView)itemView.findViewById(R.id.judul_buku);
            nomor_pemesanan_unik = (TextView)itemView.findViewById(R.id.nomor_pemesanan_unik);
            card_view = (CardView)itemView.findViewById(R.id.card_view);
            status_booking = (FancyButton) itemView.findViewById(R.id.status_booking);
        }
    }

    @Override
    public AdapterAllTransactionPemesanan.TransactionResultsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_transaction_pemesanan_item, parent, false);

        return new TransactionResultsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AdapterAllTransactionPemesanan.TransactionResultsViewHolder holder, int position) {
        final Transaksi transaksi = transaksiList.get(position);
        String get_status = transaksi.getStatus();
        holder.judul_buku.setText(transaksi.getJudul_buku());
        holder.nomor_pemesanan_unik.setText(transaksi.getNomor_pemesanan_unik());
        if (get_status.equals("waiting")) {
            holder.status_booking.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            holder.status_booking.setFocusBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            holder.status_booking.setText("Menunggu");
        } else if (get_status.equals("expired")) {
            holder.status_booking.setBackgroundColor(mContext.getResources().getColor(R.color.red));
            holder.status_booking.setFocusBackgroundColor(mContext.getResources().getColor(R.color.dark_red));
            holder.status_booking.setText("Kadaluwarsa");
        } else if (get_status.equals("success")) {
            holder.status_booking.setBackgroundColor(mContext.getResources().getColor(R.color.green));
            holder.status_booking.setFocusBackgroundColor(mContext.getResources().getColor(R.color.dark_green));
            holder.status_booking.setText("Sukses");
        }

        Glide.with(mContext)
                .load(URLs.base_url_web.toString() + transaksi.getThumbnail())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.thumbnail_buku);

        holder.status_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDetailTransaction(transaksi.getNomor_pemesanan_unik(), transaksi.getId_pemesanan());
            }
        });

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDetailTransaction(transaksi.getNomor_pemesanan_unik(), transaksi.getId_pemesanan());
            }
        });

        holder.judul_buku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDetailTransaction(transaksi.getNomor_pemesanan_unik(), transaksi.getId_pemesanan());
            }
        });

        holder.thumbnail_buku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDetailTransaction(transaksi.getNomor_pemesanan_unik(), transaksi.getId_pemesanan());
            }
        });
    }
    public void openDetailTransaction(String nomor_pemesanan_unik, String id_pemesanan){
        Intent intent = new Intent(mContext, DetailTransaksiActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("nomor_pemesanan_unik", nomor_pemesanan_unik);
        intent.putExtra("id_pemesanan", id_pemesanan);
        mContext.startActivity(intent);
    }
    @Override
    public int getItemCount() {
        return transaksiList.size();
    }

}
