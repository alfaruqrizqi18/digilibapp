package digilibapp.morfin.com.digilibapp.detail_activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.ticket_view.TicketView;
import digilibapp.morfin.com.digilibapp.url.URLs;
import digilibapp.morfin.com.digilibapp.volley.VolleySingleton;
import mehdi.sakout.fancybuttons.FancyButton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailTransaksiActivity extends AppCompatActivity {
    String get_tgl_pemesanan, get_pemesanan_dimulai_pada_jam, get_pemesanan_hangus_pada_jam, get_status,
            get_judul_buku, get_thumbnail_url;
    String nomor_pemesanan_unik_val, id_pemesanan_val;
    TextView judul_buku, nomor_pemesanan_unik, jam_kadaluwarsa, tgl_pemesanan, tv1;
    FancyButton status_booking;
    Button btn_cancel_booking;
    ImageView thumbnail;
    ProgressBar progressBar;
    TicketView layout_ticket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaksi);

        nomor_pemesanan_unik_val = getIntent().getStringExtra("nomor_pemesanan_unik").toString().trim();
        id_pemesanan_val = getIntent().getStringExtra("id_pemesanan").toString().trim();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Transaksi Pemesanan");

        layout_ticket = (TicketView)findViewById(R.id.layout_ticket);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        thumbnail = (ImageView)findViewById(R.id.thumbnail);
        tv1 = (TextView)findViewById(R.id.tv1);
        judul_buku = (TextView)findViewById(R.id.judul_buku);
        nomor_pemesanan_unik = (TextView)findViewById(R.id.nomor_pemesanan_unik);
        jam_kadaluwarsa = (TextView)findViewById(R.id.jam_kadaluwarsa);
        tgl_pemesanan = (TextView)findViewById(R.id.tgl_pemesanan);
        status_booking = (FancyButton)findViewById(R.id.status_booking);
        btn_cancel_booking = (Button)findViewById(R.id.btn_cancel_booking);

        starterPack();
    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS


    @Override
    protected void onResume() {
        super.onResume();
        showDetailPemesanan();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void starterPack(){
        nomor_pemesanan_unik.setText(nomor_pemesanan_unik_val);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void beforeLoad(){
        progressBar.setVisibility(View.VISIBLE);
        layout_ticket.setVisibility(View.GONE);
        tv1.setVisibility(View.GONE);
        btn_cancel_booking.setVisibility(View.GONE);
    }

    private void showResult(){
        progressBar.setVisibility(View.GONE);
        layout_ticket.setVisibility(View.VISIBLE);
        tv1.setVisibility(View.VISIBLE);
        btn_cancel_booking.setVisibility(View.VISIBLE);
    }

    private void showDetailPemesanan(){
        beforeLoad();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.detail_transaction+id_pemesanan_val,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showResult();
                        try {
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            String time_remaining = responses.getString("time_remaining");
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            Date tgl_pemesanan_date = new Date();

                            for (int i = 0; i < results.length(); i++) {
                                JSONObject data = results.getJSONObject(i);
                                get_tgl_pemesanan = data.getString("tgl_pemesanan").toString().trim();
                                try {
                                    tgl_pemesanan_date = simpleDateFormat.parse(get_tgl_pemesanan);
                                    simpleDateFormat.applyPattern("d MMM yyyy");
                                    tgl_pemesanan.setText(simpleDateFormat.format(tgl_pemesanan_date));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                get_pemesanan_dimulai_pada_jam = data.getString("pemesanan_dimulai_pada_jam");
                                get_pemesanan_hangus_pada_jam = data.getString("pemesanan_hangus_pada_jam");
                                get_status = data.getString("status");
                                get_judul_buku = data.getString("judul_buku");
                                get_thumbnail_url = data.getString("thumbnail");

                                judul_buku.setText(get_judul_buku);
                                Glide.with(getApplicationContext()).load(URLs.base_url_web.toString() + get_thumbnail_url).into(thumbnail);
                                jam_kadaluwarsa.setText(time_remaining);
                                if (get_status.equals("waiting")) {
                                    status_booking.setText("Menunggu");
                                    status_booking.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                                    status_booking.setFocusBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                                    btn_cancel_booking.setText("Batalkan pemesanan");
                                    btn_cancel_booking.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            new SweetAlertDialog(DetailTransaksiActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                .setContentText("Apa kamu yakin ?")
                                                .setConfirmText("Ya")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        cancel_booking();
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .setCancelButton("Tidak", new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                }).show();
                                        }
                                    });
                                } else if (get_status.equals("expired")) {
                                    status_booking.setText("Kadaluwarsa");
                                    status_booking.setBackgroundColor(getResources().getColor(R.color.red));
                                    status_booking.setFocusBackgroundColor(getResources().getColor(R.color.dark_red));
                                    btn_cancel_booking.setText("Tutup jendela ini");
                                    btn_cancel_booking.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    btn_cancel_booking.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finish();
                                        }
                                    });
                                } else if (get_status.equals("success")) {
                                    status_booking.setText("Sukses");
                                    status_booking.setBackgroundColor(getResources().getColor(R.color.green));
                                    status_booking.setFocusBackgroundColor(getResources().getColor(R.color.dark_green));
                                    btn_cancel_booking.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                    btn_cancel_booking.setText("Tutup jendela ini");
                                    btn_cancel_booking.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finish();
                                        }
                                    });
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void cancel_booking(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.cancel_transaction,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_pemesanan", id_pemesanan_val);
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
