package digilibapp.morfin.com.digilibapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.detail_activity.DetailBookActivity;
import digilibapp.morfin.com.digilibapp.models.Buku;
import digilibapp.morfin.com.digilibapp.url.URLs;

/**
 * Created by Morfin on 1/25/2018.
 */

public class AdapterResultsSearchBooks1 extends RecyclerView.Adapter<AdapterResultsSearchBooks1.BookResults1ViewHolder> {
    private Context mContext;
    private List<Buku> bukuList;

    public AdapterResultsSearchBooks1(Context mContext, List<Buku> bukuList) {
        this.mContext = mContext;
        this.bukuList = bukuList;
    }

    public class BookResults1ViewHolder extends RecyclerView.ViewHolder{
        public ImageView thumbnail_buku;
        public TextView judul_buku, jurusan;
        CardView card_view_books;
        public BookResults1ViewHolder(View itemView) {
            super(itemView);
            thumbnail_buku = (ImageView)itemView.findViewById(R.id.thumbnail_buku);
            judul_buku = (TextView)itemView.findViewById(R.id.judul_buku);
            jurusan = (TextView)itemView.findViewById(R.id.jurusan);
            card_view_books = (CardView)itemView.findViewById(R.id.card_view_books);
        }
    }

    @Override
    public AdapterResultsSearchBooks1.BookResults1ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_results1_books_item, parent, false);

        return new BookResults1ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AdapterResultsSearchBooks1.BookResults1ViewHolder holder, int position) {
        final Buku buku = bukuList.get(position);
        holder.judul_buku.setText(buku.getJudul_buku());
        holder.jurusan.setText(buku.getJurusan());
        Glide.with(mContext)
                .load(URLs.base_url_web.toString() + buku.getThumbnail())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.thumbnail_buku);

        holder.card_view_books.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailBookActivity.class);
                intent.putExtra("id_buku", buku.getId_buku());
                intent.putExtra("judul_buku", buku.getJudul_buku());
                mContext.startActivity(intent);
            }
        });
        holder.judul_buku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailBookActivity.class);
                intent.putExtra("id_buku", buku.getId_buku());
                intent.putExtra("judul_buku", buku.getJudul_buku());
                mContext.startActivity(intent);
            }
        });
        holder.thumbnail_buku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailBookActivity.class);
                intent.putExtra("id_buku", buku.getId_buku());
                intent.putExtra("judul_buku", buku.getJudul_buku());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bukuList.size();
    }


}
