package digilibapp.morfin.com.digilibapp.detail_activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.rxdownloader.RxDownloader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import cn.pedant.SweetAlert.SweetAlertDialog;
import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.url.URLs;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import mehdi.sakout.fancybuttons.FancyButton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailLaporanActivity extends AppCompatActivity {
    String id_laporan;
    String judul_laporan;
    String tahun;
    String abstrak;
    String kata_kunci;
    String jenis_laporan;
    String alamat_file_laporan;
    String jurusan;
    String thumbnail;

    ImageView thumbnail_view;
    TextView title_view, jurusan_view, jenis_laporan_view, abstrak_view, kata_kunci_view;
    FancyButton btn_download;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_laporan);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title_view = findViewById(R.id.title_view);
        jurusan_view = findViewById(R.id.jurusan_view);
        jenis_laporan_view = findViewById(R.id.jenis_laporan_view);
        abstrak_view = findViewById(R.id.abstrak_view);
        thumbnail_view = findViewById(R.id.thumbnail_view);
        kata_kunci_view = findViewById(R.id.kata_kunci_view);
        btn_download = findViewById(R.id.btn_download);


        id_laporan = getIntent().getStringExtra("id_laporan").toString().trim();
        judul_laporan = getIntent().getStringExtra("judul_laporan").toString().trim();
        tahun = getIntent().getStringExtra("tahun").toString().trim();
        abstrak = getIntent().getStringExtra("abstrak").toString().trim();
        kata_kunci = getIntent().getStringExtra("kata_kunci").toString().trim();
        jenis_laporan = getIntent().getStringExtra("jenis_laporan").toString().trim();
        alamat_file_laporan = getIntent().getStringExtra("alamat_file_laporan").toString().trim();
        jurusan = getIntent().getStringExtra("jurusan").toString().trim();
        thumbnail = getIntent().getStringExtra("thumbnail").toString().trim();
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setSubtitle(judul_laporan);
        load_detail_laporan();
        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choice_dialog(alamat_file_laporan, judul_laporan);
            }
        });
    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void load_detail_laporan(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            jurusan_view.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_icon_trophy, 0, 0, 0);
            jurusan_view.setCompoundDrawablePadding(10);
            jenis_laporan_view.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_icon_clip, 0, 0, 0);
            jenis_laporan_view.setCompoundDrawablePadding(10);
        }
        Glide.with(getApplicationContext())
                .load(URLs.base_url_web.toString() + thumbnail)
                .into(thumbnail_view);
        title_view.setText(judul_laporan);
        jurusan_view.setText(jurusan);
        jenis_laporan_view.setText(jenis_laporan);
        kata_kunci_view.setText(kata_kunci);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            abstrak_view.setText(Html.fromHtml(abstrak,Html.FROM_HTML_MODE_LEGACY));
        } else {
            abstrak_view.setText(Html.fromHtml(abstrak));
        }
    }

    private void choice_dialog(final String url, final String judul_laporan){
        new SweetAlertDialog(DetailLaporanActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setCancelButton("Tidak", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .setContentText("Ingin mengunduh ?")
                .setConfirmButton("Ya, unduh", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                            download_files(url, judul_laporan + ".docx", "application/msword");
                        } else if (url.toString().contains(".pdf")){
                            download_files(url, judul_laporan + ".pdf","application/pdf");
                        }
                        sDialog.dismissWithAnimation();
                    }
                }).show();
    }

    private void download_files(final String url, String filename, final String mimeType){
        RxDownloader rxDownloader = new RxDownloader(DetailLaporanActivity.this);
        rxDownloader.download(url, filename, mimeType, true)
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String filePath) {
                        try {
                            URL urls = new URL(filePath.replaceAll("%20", " "));
                            Toast.makeText(getApplicationContext(), "Lokasi file : "+ filePath, Toast.LENGTH_LONG).show();
                            File temp_file = new File(urls.getFile());
                            Intent intent = new Intent("android.intent.action.VIEW");

                            if (mimeType.equals("application/msword")) {
                                intent.setDataAndType(Uri.fromFile(temp_file),"application/msword");
                                startActivity(intent);
                            } else {
                                intent.setDataAndType(Uri.fromFile(temp_file),"application/pdf");
                                startActivity(intent);
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
