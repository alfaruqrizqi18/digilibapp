package digilibapp.morfin.com.digilibapp.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;

import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.models.LogActivity;

/**
 * Created by Morfin on 3/16/2018.
 */

public class AdapterLogActivity extends RecyclerView.Adapter<AdapterLogActivity.MyViewHolder> {
    private List<LogActivity> logActivityList;
    private Context mContext;

    public AdapterLogActivity(Context mContext, List<LogActivity> logActivityList) {
        this.mContext = mContext;
        this.logActivityList = logActivityList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView judul_log, judul_buku, isi_log, tgl_log;
        FrameLayout card_edge;
        public MyViewHolder(View itemView) {
            super(itemView);
            judul_log = itemView.findViewById(R.id.judul_log);
            judul_buku = itemView.findViewById(R.id.judul_buku);
            isi_log = itemView.findViewById(R.id.isi_log);
            tgl_log = itemView.findViewById(R.id.tgl_log);
            card_edge = itemView.findViewById(R.id.card_edge);
        }
    }

    @Override
    public AdapterLogActivity.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.log_activity_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final LogActivity logActivity = logActivityList.get(position);
        String get_type_log = logActivity.getType_log().toString();

        if (get_type_log.equals("1")) {
            holder.card_edge.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
        } else if (get_type_log.equals("2")) {
            holder.card_edge.setBackgroundColor(mContext.getResources().getColor(R.color.green));
//            card_edge_drawables = mContext.getResources().getDrawable(R.drawable.card_edge_green);
        } else if (get_type_log.equals("3")) {
            holder.card_edge.setBackgroundColor(mContext.getResources().getColor(R.color.dot_dark_screen3));
//            card_edge_drawables = mContext.getResources().getDrawable(R.drawable.card_edge_orange);
        }

        holder.judul_log.setText(logActivity.getJudul_log());
        holder.isi_log.setText(logActivity.getIsi_log());
        holder.judul_buku.setText(logActivity.getJudul_buku());
        holder.tgl_log.setText(logActivity.getTgl_log());
    }

    @Override
    public int getItemCount() {
        return logActivityList.size();
    }

}
