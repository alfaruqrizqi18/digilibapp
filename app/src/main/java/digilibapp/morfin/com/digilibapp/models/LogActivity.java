package digilibapp.morfin.com.digilibapp.models;

/**
 * Created by Morfin on 3/16/2018.
 */

public class LogActivity {
    String id_log, judul_log, isi_log, id_stok, tgl_log, type_log, judul_buku;

    public String getId_log() {
        return id_log;
    }

    public void setId_log(String id_log) {
        this.id_log = id_log;
    }

    public String getJudul_log() {
        return judul_log;
    }

    public void setJudul_log(String judul_log) {
        this.judul_log = judul_log;
    }

    public String getIsi_log() {
        return isi_log;
    }

    public void setIsi_log(String isi_log) {
        this.isi_log = isi_log;
    }

    public String getId_stok() {
        return id_stok;
    }

    public void setId_stok(String id_stok) {
        this.id_stok = id_stok;
    }

    public String getTgl_log() {
        return tgl_log;
    }

    public void setTgl_log(String tgl_log) {
        this.tgl_log = tgl_log;
    }

    public String getType_log() {
        return type_log;
    }

    public void setType_log(String type_log) {
        this.type_log = type_log;
    }

    public String getJudul_buku() {
        return judul_buku;
    }

    public void setJudul_buku(String judul_buku) {
        this.judul_buku = judul_buku;
    }
}
