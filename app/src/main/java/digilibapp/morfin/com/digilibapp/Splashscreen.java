package digilibapp.morfin.com.digilibapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import digilibapp.morfin.com.digilibapp.sharedpreference.SharedPrefManager;

public class Splashscreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        if (SharedPrefManager.getInstance(getApplicationContext()).isLoggedIn()) {
            try {
                Thread.sleep(1000);
                startActivity(new Intent(Splashscreen.this, MainActivityDrawer.class));
                finish();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            try {
                Thread.sleep(3000);
                startActivity(new Intent(Splashscreen.this, IntroActivity.class));
                finish();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
