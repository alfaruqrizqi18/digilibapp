package digilibapp.morfin.com.digilibapp.nav_menu_activity;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.adapter.AdapterAllTransactionPemesanan;
import digilibapp.morfin.com.digilibapp.models.Transaksi;
import digilibapp.morfin.com.digilibapp.sharedpreference.SharedPrefManager;
import digilibapp.morfin.com.digilibapp.url.URLs;
import digilibapp.morfin.com.digilibapp.volley.VolleySingleton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TransaksiPemesananActivity extends AppCompatActivity {
    String get_id_pemesanan, get_nomor_pemesanan_unik, get_status, get_judul_buku, get_thumbnail;
    SweetAlertDialog sweetAlertDialog;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    AdapterAllTransactionPemesanan adapterAllTransactionPemesanan;
    ProgressBar progressBar;
    String url_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaksi_pemesanan);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Transaksi Pemesanan");
//        sweetAlertDialog = new SweetAlertDialog(TransaksiPemesananActivity.this, SweetAlertDialog.PROGRESS_TYPE);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

        starterPack();
    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate dari menu; disini akan menambahkan item menu pada Actionbar
        getMenuInflater().inflate(R.menu.menu_pemesanan, menu);//Memanggil file bernama menu di folder menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.waiting:
                showAllTransactionByStatus("waiting");
                return true;
            case R.id.success:
                showAllTransactionByStatus("success");
                return true;
            case R.id.expired:
                showAllTransactionByStatus("expired");
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        starterPack();
    }

    @Override
    protected void onStop() {
        if (recyclerView != null) {
            recyclerView.setAdapter(null);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (recyclerView != null) {
            recyclerView.setAdapter(null);
        }
    }

    private void starterPack(){
        showAllTransaction();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            private boolean scrollEnabled;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ?
                                0 : recyclerView.getChildAt(0).getTop();

                boolean newScrollEnabled =
                        (dx == 0 && topRowVerticalPosition >= 0) ?
                                true : false;

                if (null != swipeRefreshLayout && scrollEnabled != newScrollEnabled) {
                    // Start refreshing....
                    swipeRefreshLayout.setEnabled(newScrollEnabled);
                    scrollEnabled = newScrollEnabled;
                }
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showAllTransaction();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void showAllTransaction(){
        progressBar.setVisibility(View.VISIBLE);
        final String nim = SharedPrefManager.getInstance(TransaksiPemesananActivity.this).showSession().get("NIM").toString();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.all_transaction_pemesanan_by_nim + nim,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<Transaksi> dataTransaksi = new ArrayList<>();
                        try {
                            dataTransaksi.clear();
                            recyclerView.setLayoutManager(null);
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            for (int i = 0; i < results.length(); i++) {
                                JSONObject data = results.getJSONObject(i);
                                Transaksi transaksi = new Transaksi();
                                get_id_pemesanan = data.getString("id_pemesanan");
                                get_judul_buku = data.getString("judul_buku");
                                get_nomor_pemesanan_unik = data.getString("nomor_pemesanan_unik");
                                get_status = data.getString("status");
                                get_thumbnail = data.getString("thumbnail");
                                transaksi.setId_pemesanan(get_id_pemesanan);
                                transaksi.setJudul_buku(get_judul_buku);
                                transaksi.setNomor_pemesanan_unik(get_nomor_pemesanan_unik);
                                transaksi.setStatus(get_status);
                                transaksi.setThumbnail(get_thumbnail);
                                dataTransaksi.add(transaksi);
                            }
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            adapterAllTransactionPemesanan = new AdapterAllTransactionPemesanan(getApplicationContext(), dataTransaksi);
                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setAdapter(adapterAllTransactionPemesanan);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void showAllTransactionByStatus(String status){
        progressBar.setVisibility(View.VISIBLE);
        final String nim = SharedPrefManager.getInstance(TransaksiPemesananActivity.this).showSession().get("NIM").toString();
        if (status.equals("waiting")){
            url_status = URLs.all_transaction_pemesanan_by_nim_filter_waiting.toString();
        } else if(status.equals("success")){
            url_status = URLs.all_transaction_pemesanan_by_nim_filter_success.toString();
        } else if(status.equals("expired")) {
            url_status = URLs.all_transaction_pemesanan_by_nim_filter_expired.toString();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url_status + nim,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<Transaksi> dataTransaksi = new ArrayList<>();
                        try {
                            dataTransaksi.clear();
                            recyclerView.setLayoutManager(null);
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            for (int i = 0; i < results.length(); i++) {
                                JSONObject data = results.getJSONObject(i);
                                Transaksi transaksi = new Transaksi();
                                get_id_pemesanan = data.getString("id_pemesanan");
                                get_judul_buku = data.getString("judul_buku");
                                get_nomor_pemesanan_unik = data.getString("nomor_pemesanan_unik");
                                get_status = data.getString("status");
                                get_thumbnail = data.getString("thumbnail");
                                transaksi.setId_pemesanan(get_id_pemesanan);
                                transaksi.setJudul_buku(get_judul_buku);
                                transaksi.setNomor_pemesanan_unik(get_nomor_pemesanan_unik);
                                transaksi.setStatus(get_status);
                                transaksi.setThumbnail(get_thumbnail);
                                dataTransaksi.add(transaksi);
                            }
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            adapterAllTransactionPemesanan = new AdapterAllTransactionPemesanan(getApplicationContext(), dataTransaksi);
                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setAdapter(adapterAllTransactionPemesanan);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
