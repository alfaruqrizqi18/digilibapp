package digilibapp.morfin.com.digilibapp.models;

/**
 * Created by Morfin on 2/10/2018.
 */

public class Transaksi {

    String id_pemesanan;

    public Transaksi() {
    }

    String nomor_pemesanan_unik;
    String tgl_pemesanan;
    String pemesanan_dimulai_pada_jam;
    String pemesanan_hangus_pada_jam;
    String status;
    String judul_buku;
    String thumbnail;

    public String getId_pemesanan() {
        return id_pemesanan;
    }

    public void setId_pemesanan(String id_pemesanan) {
        this.id_pemesanan = id_pemesanan;
    }

    public String getNomor_pemesanan_unik() {
        return nomor_pemesanan_unik;
    }

    public void setNomor_pemesanan_unik(String nomor_pemesanan_unik) {
        this.nomor_pemesanan_unik = nomor_pemesanan_unik;
    }

    public String getTgl_pemesanan() {
        return tgl_pemesanan;
    }

    public void setTgl_pemesanan(String tgl_pemesanan) {
        this.tgl_pemesanan = tgl_pemesanan;
    }

    public String getPemesanan_dimulai_pada_jam() {
        return pemesanan_dimulai_pada_jam;
    }

    public void setPemesanan_dimulai_pada_jam(String pemesanan_dimulai_pada_jam) {
        this.pemesanan_dimulai_pada_jam = pemesanan_dimulai_pada_jam;
    }

    public String getPemesanan_hangus_pada_jam() {
        return pemesanan_hangus_pada_jam;
    }

    public void setPemesanan_hangus_pada_jam(String pemesanan_hangus_pada_jam) {
        this.pemesanan_hangus_pada_jam = pemesanan_hangus_pada_jam;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJudul_buku() {
        return judul_buku;
    }

    public void setJudul_buku(String judul_buku) {
        this.judul_buku = judul_buku;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

}
