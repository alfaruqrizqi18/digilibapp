package digilibapp.morfin.com.digilibapp.font_replacer;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.view.WindowManager;

import digilibapp.morfin.com.digilibapp.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Morfin on 3/15/2018.
 */

public class FontReplacer extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/SanFranciscoText-Regular.otf")
            .setFontAttrId(R.attr.fontPath).build()
        );

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);

    }


}
