package digilibapp.morfin.com.digilibapp.nav_menu_activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.adapter.AdapterLogActivity;
import digilibapp.morfin.com.digilibapp.models.LogActivity;
import digilibapp.morfin.com.digilibapp.sharedpreference.SharedPrefManager;
import digilibapp.morfin.com.digilibapp.url.URLs;
import digilibapp.morfin.com.digilibapp.volley.VolleySingleton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileActivity extends AppCompatActivity {
    String sharedPrefNama, sharedPrefJurusan;
    TextView header_nama, header_jurusan, mTitle, counter_transaksi, counter_pemesanan, counter_denda;
    Toolbar toolbar;
    AppBarLayout appBarLayout;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    CollapsingToolbarLayout collapsingToolbar;
    AdapterLogActivity adapterLogActivity;
    View oops;
    int mutedColor = R.attr.colorPrimary;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        sharedPrefNama = SharedPrefManager.getInstance(ProfileActivity.this).showSession().get("NAMA").toString();
        sharedPrefJurusan = SharedPrefManager.getInstance(ProfileActivity.this).showSession().get("JURUSAN").toString();
        toolbar = findViewById(R.id.toolbar);
        mTitle = toolbar.findViewById(R.id.toolbar_title);
        appBarLayout = findViewById(R.id.appbar);
        header_nama = findViewById(R.id.header_nama);
        header_jurusan = findViewById(R.id.header_jurusan);
        counter_transaksi = findViewById(R.id.counter_transaksi);
        counter_pemesanan = findViewById(R.id.counter_pemesanan);
        counter_denda = findViewById(R.id.counter_denda);
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.recyclerView);
        oops = findViewById(R.id.oops);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // pengaturan dan inisialisasi collapsing toolbar
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(null);
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.colorPrimary));
        collapsingToolbar.setTitleEnabled(false);
        starterPack();

    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS

    private void starterPack(){
        oops.setVisibility(View.GONE);
        get_counter_and_log_activity();
        recyclerView.setNestedScrollingEnabled(false);
        header_nama.setText(sharedPrefNama);
        header_jurusan.setText(sharedPrefJurusan);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isVisible = true;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    mTitle.setText(sharedPrefNama);
//                    toolbar.setSubtitle(sharedPrefJurusan);
                    isVisible = true;
                } else if(isVisible) {
                    mTitle.setText("");
                    toolbar.setSubtitle("");
                    isVisible = false;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profil_menu, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data.getStringExtra("status").equals("after_change_password")) {

        } else if (data.getStringExtra("status").equals("logout")) {
            Intent intent = new Intent();
            intent.putExtra("status", "logout");
            setResult(1,intent);
            finish();
        } else if (data.getStringExtra("status").equals("back_pressed")) {

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("status", "back_pressed");
        setResult(1,intent);
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            intent.putExtra("status", "back_pressed");
            setResult(1,intent);
            finish();
        } else if (menuItem.getItemId() == R.id.pengaturan) {
            Intent intent = new Intent(ProfileActivity.this, PengaturanActvity.class);
            startActivityForResult(intent, 1);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void before_load(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void after_load(){
        progressBar.setVisibility(View.GONE);
    }

    private static char[] c = new char[]{'k', 'm', 'b', 't'};
    private static String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) %10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
        return (d < 1000? //this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99)? //this decides whether to trim the decimals
                        (int) d * 10 / 10 : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration+1));

    }

    private void get_counter_and_log_activity(){
        before_load();
        final String nim = SharedPrefManager.getInstance(ProfileActivity.this).showSession().get("NIM").toString();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.profile_url + nim,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        after_load();
                        List<LogActivity> dataLog = new ArrayList<>();
                        try {
                            dataLog.clear();
                            recyclerView.setLayoutManager(null);
                            JSONObject responses = new JSONObject(response);
                            JSONArray results_log = responses.getJSONArray("results_log");
                            JSONArray results_counter = responses.getJSONArray("results_counter");

                            JSONObject data_counter = results_counter.getJSONObject(0);
                            counter_transaksi.setText(data_counter.getString("total_transaksi"));
                            counter_pemesanan.setText(data_counter.getString("total_pemesanan"));
                            if (data_counter.getString("total_denda").equals("null")) {
                                counter_denda.setText("0k");
                            } else {
                                counter_denda.setText(coolFormat(Double.parseDouble(data_counter.getString("total_denda")), 0));
                            }

                            if (results_log.length() < 1 ){
                                oops.setVisibility(View.VISIBLE);
                            } else {
                                oops.setVisibility(View.GONE);
                                for (int i = 0; i < results_log.length(); i++) {
                                    JSONObject data = results_log.getJSONObject(i);
                                    LogActivity logActivity = new LogActivity();
                                    logActivity.setId_log(data.getString("id_log"));
                                    logActivity.setId_stok(data.getString("id_stok"));
                                    logActivity.setJudul_buku(data.getString("judul_buku"));
                                    logActivity.setIsi_log(data.getString("isi_log"));
                                    logActivity.setJudul_log(data.getString("judul_log"));
                                    logActivity.setTgl_log(data.getString("tgl_log"));
                                    logActivity.setType_log(data.getString("type_log"));
                                    dataLog.add(logActivity);
                                    Log.d("Notifikasi ", String.valueOf(logActivity));
                                }
                            }
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            adapterLogActivity = new AdapterLogActivity(getApplicationContext(), dataLog);
                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setAdapter(adapterLogActivity);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
