package digilibapp.morfin.com.digilibapp.nav_menu_activity;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.adapter.AdapterAllTransactionPeminjaman;
import digilibapp.morfin.com.digilibapp.models.TransaksiPeminjaman;
import digilibapp.morfin.com.digilibapp.sharedpreference.SharedPrefManager;
import digilibapp.morfin.com.digilibapp.url.URLs;
import digilibapp.morfin.com.digilibapp.volley.VolleySingleton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TransaksiPeminjamanActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    AdapterAllTransactionPeminjaman adapterAllTransactionPeminjaman;
    ProgressBar progressBar;
    String url_status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaksi_peminjaman);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Transaksi Peminjaman");


        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        starterPack();
    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate dari menu; disini akan menambahkan item menu pada Actionbar
        getMenuInflater().inflate(R.menu.menu_peminjaman, menu);//Memanggil file bernama menu di folder menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.berlangsung:
                showAllPeminjamanByStatus("peminjaman_sedang_berlangsung");
                return true;
            case R.id.berakhir:
                showAllPeminjamanByStatus("peminjaman_telah_berakhir");
                return true;
            case R.id.lewat_batas:
                showAllPeminjamanByStatus("peminjaman_melewati_batas");
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void starterPack(){
        showAllPeminjaman();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            private boolean scrollEnabled;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ?
                                0 : recyclerView.getChildAt(0).getTop();

                boolean newScrollEnabled =
                        (dx == 0 && topRowVerticalPosition >= 0) ?
                                true : false;

                if (null != swipeRefreshLayout && scrollEnabled != newScrollEnabled) {
                    // Start refreshing....
                    swipeRefreshLayout.setEnabled(newScrollEnabled);
                    scrollEnabled = newScrollEnabled;
                }
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showAllPeminjaman();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (recyclerView != null) {
            recyclerView.setAdapter(null);
        }
    }

    private void showAllPeminjaman(){
        progressBar.setVisibility(View.VISIBLE);
        final String nim = SharedPrefManager.getInstance(TransaksiPeminjamanActivity.this).showSession().get("NIM").toString();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.all_transaction_peminjaman_by_nim + nim,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<TransaksiPeminjaman> dataTransaksi = new ArrayList<>();
                        try {
                            dataTransaksi.clear();
                            recyclerView.setLayoutManager(null);
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            for (int i = 0; i < results.length(); i++) {
                                JSONObject data = results.getJSONObject(i);
                                TransaksiPeminjaman transaksiPeminjaman = new TransaksiPeminjaman();
                                transaksiPeminjaman.setId_transaksi(data.getString("id_transaksi"));
                                transaksiPeminjaman.setJudul_buku(data.getString("judul_buku"));
                                transaksiPeminjaman.setStatus_transaksi(data.getString("status_transaksi"));
                                transaksiPeminjaman.setThumbnail(data.getString("thumbnail"));
                                dataTransaksi.add(transaksiPeminjaman);
                            }
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            adapterAllTransactionPeminjaman = new AdapterAllTransactionPeminjaman(getApplicationContext(), dataTransaksi);
                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setAdapter(adapterAllTransactionPeminjaman);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void showAllPeminjamanByStatus(String status){
        progressBar.setVisibility(View.VISIBLE);
        if (status.equals("peminjaman_sedang_berlangsung")) {
            url_status = URLs.all_transaction_peminjaman_by_nim_berlangsung.toString();
        } else if (status.equals("peminjaman_telah_berakhir")) {
            url_status = URLs.all_transaction_peminjaman_by_nim_berakhir.toString();
        } else if (status.equals("peminjaman_melewati_batas")) {
            url_status = URLs.all_transaction_peminjaman_by_nim_lewat_batas.toString();
        }
        final String nim = SharedPrefManager.getInstance(TransaksiPeminjamanActivity.this).showSession().get("NIM").toString();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url_status + nim,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        List<TransaksiPeminjaman> dataTransaksi = new ArrayList<>();
                        try {
                            dataTransaksi.clear();
                            recyclerView.setLayoutManager(null);
                            JSONObject responses = new JSONObject(response);
                            JSONArray results = responses.getJSONArray("results");
                            for (int i = 0; i < results.length(); i++) {
                                JSONObject data = results.getJSONObject(i);
                                TransaksiPeminjaman transaksiPeminjaman = new TransaksiPeminjaman();
                                transaksiPeminjaman.setId_transaksi(data.getString("id_transaksi"));
                                transaksiPeminjaman.setJudul_buku(data.getString("judul_buku"));
                                transaksiPeminjaman.setStatus_transaksi(data.getString("status_transaksi"));
                                transaksiPeminjaman.setThumbnail(data.getString("thumbnail"));
                                dataTransaksi.add(transaksiPeminjaman);
                            }
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            adapterAllTransactionPeminjaman = new AdapterAllTransactionPeminjaman(getApplicationContext(), dataTransaksi);
                            recyclerView.setLayoutManager(mLayoutManager);
                            recyclerView.setAdapter(adapterAllTransactionPeminjaman);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
// Add the request to the RequestQueue.
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
