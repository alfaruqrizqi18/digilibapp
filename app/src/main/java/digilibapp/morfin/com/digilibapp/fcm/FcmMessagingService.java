package digilibapp.morfin.com.digilibapp.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import digilibapp.morfin.com.digilibapp.R;

/**
 * Created by Morfin on 3/9/2018.
 */

public class FcmMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        super.onMessageReceived(remoteMessage);
        String id_notifikasi = remoteMessage.getData().get("id_notifikasi");
        String judul_notifikasi = remoteMessage.getData().get("judul_notifikasi");
        String pesan_notifikasi = remoteMessage.getData().get("pesan_notifikasi");
        String tgl_notifikasi = remoteMessage.getData().get("tgl_notifikasi");
        String title = remoteMessage.getNotification().getTitle();
        String message = remoteMessage.getNotification().getBody();
        String click_action = remoteMessage.getNotification().getClickAction();
        Intent intent = new Intent(click_action);
        intent.putExtra("id_notifikasi", id_notifikasi);
        intent.putExtra("judul_notifikasi", judul_notifikasi);
        intent.putExtra("pesan_notifikasi", pesan_notifikasi);
        intent.putExtra("tgl_notifikasi", tgl_notifikasi);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notification_builder = new NotificationCompat.Builder(this);
        notification_builder.setContentTitle(title);
        notification_builder.setContentText(message);
        notification_builder.setSmallIcon(R.mipmap.ic_launcher_logo);
        notification_builder.setAutoCancel(true);
        notification_builder.setContentIntent(pendingIntent);

        NotificationManager notificationManager = ( NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification_builder.build());
    }
}
