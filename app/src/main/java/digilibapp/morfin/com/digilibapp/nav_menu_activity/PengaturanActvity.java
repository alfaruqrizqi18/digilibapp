package digilibapp.morfin.com.digilibapp.nav_menu_activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import digilibapp.morfin.com.digilibapp.MainActivityDrawer;
import digilibapp.morfin.com.digilibapp.R;
import digilibapp.morfin.com.digilibapp.firebase_token_service.FirebaseToken;
import digilibapp.morfin.com.digilibapp.models.Mahasiswa;
import digilibapp.morfin.com.digilibapp.sharedpreference.SharedPrefManager;
import digilibapp.morfin.com.digilibapp.url.URLs;
import digilibapp.morfin.com.digilibapp.volley.VolleySingleton;
import mehdi.sakout.fancybuttons.FancyButton;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PengaturanActvity extends AppCompatActivity {
    boolean is_open, is_after_change_password;
    EditText pwd_lama, pwd_baru_1, pwd_baru_2;
    TextView ubah_kata_sandi;
    FancyButton btn_keluar, btn_simpan;
    CardView card_view_ubah_kata_sandi;
    View containerForm;
    String password_lama, nim;
    SweetAlertDialog sweetAlertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan_actvity);
        sweetAlertDialog = new SweetAlertDialog(PengaturanActvity.this, SweetAlertDialog.PROGRESS_TYPE);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pengaturan");

        password_lama = SharedPrefManager.getInstance(PengaturanActvity.this).showSession().get("PASSWORD").toString();
        nim = SharedPrefManager.getInstance(PengaturanActvity.this).showSession().get("NIM").toString();


        card_view_ubah_kata_sandi = (CardView)findViewById(R.id.card_view_ubah_kata_sandi);
        containerForm = (View)findViewById(R.id.containerForm);
        ubah_kata_sandi = (TextView)findViewById(R.id.ubah_kata_sandi);
        pwd_lama = (EditText)findViewById(R.id.pwd_lama);
        pwd_baru_1 = (EditText)findViewById(R.id.pwd_baru_1);
        pwd_baru_2 = (EditText)findViewById(R.id.pwd_baru_2);
        btn_simpan = (FancyButton)findViewById(R.id.btn_simpan);

        btn_keluar = (FancyButton)findViewById(R.id.btn_keluar);
        starterPack();
    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            intent.putExtra("status", "back_pressed");
            setResult(1,intent);
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    private void starterPack(){
        containerForm.setVisibility(View.GONE);
        is_open = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ubah_kata_sandi.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_icon_key, 0,0,0);
            ubah_kata_sandi.setCompoundDrawablePadding(10);
        }
        btn_keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(PengaturanActvity.this, SweetAlertDialog.WARNING_TYPE)
                        .setContentText("Apa kamu ingin keluar ?")
                        .setConfirmText("Ya")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                SharedPrefManager.getInstance(getApplicationContext()).logout();
                                FirebaseToken firebaseToken = new FirebaseToken();
                                firebaseToken.deleteFirebaseToken();
                                Intent intent = new Intent();
                                intent.putExtra("status", "logout");
                                setResult(1,intent);
                                finish();
                            }
                        })
                        .setCancelButton("Tidak", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        }).show();
            }
        });

        ubah_kata_sandi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_open) {
                    containerForm.setVisibility(View.VISIBLE);
                    is_open = false;
                } else {
                    containerForm.setVisibility(View.GONE);
                    is_open = true;
                }
            }
        });

        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                change_password_validasi();
            }
        });
    }

    private void setFormEmpty(){
        pwd_lama.setText(null);
        pwd_baru_1.setText(null);
        pwd_baru_2.setText(null);
    }

    private void change_password_validasi(){
        if (pwd_lama.getText().toString().isEmpty()
                || pwd_baru_1.getText().toString().isEmpty()
                || pwd_baru_2.getText().toString().isEmpty()) {
            new SweetAlertDialog(PengaturanActvity.this, SweetAlertDialog.ERROR_TYPE)
                    .setContentText("Mohon isi semua form fill")
                    .setConfirmText("Okay")
                    .show();
        } else {
            if (pwd_lama.getText().toString().trim().equals(password_lama)) {
                if (pwd_baru_1.getText().toString().trim().equals(pwd_baru_2.getText().toString().trim())) {
                    change_password_process();
                } else {
                    new SweetAlertDialog(PengaturanActvity.this, SweetAlertDialog.ERROR_TYPE)
                            .setContentText("Katasandi baru tidak cocok")
                            .setConfirmText("Okay")
                            .show();
                }
            } else {
                new SweetAlertDialog(PengaturanActvity.this, SweetAlertDialog.ERROR_TYPE)
                        .setContentText("Katasandi lama tidak cocok")
                        .setConfirmText("Okay")
                        .show();
            }
        }
    }

    private void change_password_process(){
        sweetAlertDialog.getProgressHelper().setBarColor(R.color.colorPrimary);
        sweetAlertDialog.setTitleText("Mohon tunggu sebentar...");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.change_password,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        sweetAlertDialog.dismiss();
                        String id_mahasiswa, nim, password, nama, jurusan, token_android;
                        //converting response to json object
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                            JSONArray results = obj.getJSONArray("results");
                            String status = obj.getString("status").toString();
                            if (status.equals("OK")) {
                                JSONObject data = results.getJSONObject(0);
                                id_mahasiswa = SharedPrefManager.getInstance(PengaturanActvity.this).showSession().get("NIM").toString();
                                nim = data.getString("nim");
                                password = data.getString("password");
                                nama = data.getString("nama");
                                jurusan = data.getString("jurusan");
                                token_android = data.getString("token_android");
                                Mahasiswa mahasiswa = new Mahasiswa(
                                        id_mahasiswa, nim, password, nama, jurusan, token_android
                                );
                                SharedPrefManager.getInstance(PengaturanActvity.this).onLogin(mahasiswa);
                                new SweetAlertDialog(PengaturanActvity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setContentText("Katasandi berhasil diubah")
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                is_after_change_password = true;
                                                Intent intent = new Intent();
                                                intent.putExtra("status", "after_change_password");
                                                setResult(1,intent);
//                                                setFormEmpty();
                                                finish();
                                                sweetAlertDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            } else if(status.equals("GAGAL")){
//                                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
//                                        .setContentText("NIM atau katasandi salah")
//                                        .setConfirmText("Okay")
//                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //getting the user from the response


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("nim", nim);
                params.put("password_baru", pwd_baru_1.getText().toString().trim());
                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("status", "back_pressed");
        setResult(1,intent);
        super.onBackPressed();
    }
}
