package digilibapp.morfin.com.digilibapp.models;

/**
 * Created by Morfin on 1/25/2018.
 */

public class Laporan {
    private String id_laporan;
    private String judul_laporan;
    private String tahun;
    private String jenis_laporan;
    private String alamat_file_laporan;
    private String jurusan;
    private String abstrak, kata_kunci;
    private String thumbnail;



    public Laporan() {

    }

    public String getId_laporan() {
        return id_laporan;
    }

    public void setId_laporan(String id_laporan) {
        this.id_laporan = id_laporan;
    }

    public String getJudul_laporan() {
        return judul_laporan;
    }

    public void setJudul_laporan(String judul_laporan) {
        this.judul_laporan = judul_laporan;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getJenis_laporan() {
        return jenis_laporan;
    }

    public void setJenis_laporan(String jenis_laporan) {
        this.jenis_laporan = jenis_laporan;
    }

    public String getAlamat_file_laporan() {
        return alamat_file_laporan;
    }

    public void setAlamat_file_laporan(String alamat_file_laporan) {
        this.alamat_file_laporan = alamat_file_laporan;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
    public String getAbstrak() {
        return abstrak;
    }

    public void setAbstrak(String abstrak) {
        this.abstrak = abstrak;
    }

    public String getKata_kunci() {
        return kata_kunci;
    }

    public void setKata_kunci(String kata_kunci) {
        this.kata_kunci = kata_kunci;
    }

}
