package digilibapp.morfin.com.digilibapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import digilibapp.morfin.com.digilibapp.fragment.FragmentBuku;
import digilibapp.morfin.com.digilibapp.nav_menu_activity.NotifikasiActivity;
import digilibapp.morfin.com.digilibapp.nav_menu_activity.PengaturanActvity;
import digilibapp.morfin.com.digilibapp.nav_menu_activity.ProfileActivity;
import digilibapp.morfin.com.digilibapp.nav_menu_activity.TransaksiPemesananActivity;
import digilibapp.morfin.com.digilibapp.nav_menu_activity.TransaksiPeminjamanActivity;
import digilibapp.morfin.com.digilibapp.sharedpreference.SharedPrefManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivityDrawer extends AppCompatActivity {
    Toolbar toolbar;
    DrawerLayout drawer;
    private Fragment fragment;
    private FragmentManager fragmentManager;
    TextView user_name, user_jurusan, datetime, hello;
    NavigationView navigationView;
    ImageView imageView;
    AppBarLayout appBarLayout;
    CollapsingToolbarLayout collapsingToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_drawer);
        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        } else {

            fragmentManager = getSupportFragmentManager();
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            imageView = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.imageView);

            appBarLayout = findViewById(R.id.appbar);
            collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
            collapsingToolbar.setTitle(null);
            collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.colorPrimary));
            collapsingToolbar.setTitleEnabled(false);


            user_name = (TextView)navigationView.getHeaderView(0).findViewById(R.id.user_name);
            user_jurusan = (TextView)navigationView.getHeaderView(0).findViewById(R.id.user_jurusan);
            datetime = (TextView)findViewById(R.id.datetime);
            hello = (TextView)findViewById(R.id.hello);
            user_name.setText(SharedPrefManager.getInstance(MainActivityDrawer.this).showSession().get("NAMA").toString());
            user_jurusan.setText(SharedPrefManager.getInstance(MainActivityDrawer.this).showSession().get("NIM").toString()+" - "+SharedPrefManager.getInstance(MainActivityDrawer.this).showSession().get("JURUSAN").toString());

            //starterPack running
            starterPack();
        }
    }

    //=====UNTUK CONFIG FONTS
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }
    //=====UNTUK CONFIG FONTS
    public void getCurrentDate() {
        Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("EEE, d MMM yyyy");
        datetime.setText(ft.format(dNow));
        String get_nama_depan = SharedPrefManager.getInstance(this).showSession().get("NAMA").toString().trim();
        String nama_depan = get_nama_depan.substring(0, get_nama_depan.indexOf(" "));
        hello.setText("Hello, "+nama_depan);
    }

    private void starterPack(){
        getCurrentDate();
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivityDrawer.this, ProfileActivity.class);
//                startActivityForResult(intent, 1);
//            }
//        });

        navigationView.getMenu().getItem(0).setChecked(true);
        fragment = new FragmentBuku();
        getBaseFragment(fragment, "Buku");
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                String title = null;
                if (id == R.id.nav_buku) {
                    fragment = new FragmentBuku();
                    title = "Buku";
                    getBaseFragment(fragment, title);
                } else if(id == R.id.nav_pemesanan) {
                    startActivity(new Intent(MainActivityDrawer.this, TransaksiPemesananActivity.class));
                } else if(id == R.id.nav_transaksi_peminjaman) {
                    startActivity(new Intent(MainActivityDrawer.this, TransaksiPeminjamanActivity.class));
                } else if (id == R.id.nav_notifikasi) {
                    startActivity(new Intent(MainActivityDrawer.this, NotifikasiActivity.class));
                }
                else if(id == R.id.nav_profil) {
                    Intent intent = new Intent(MainActivityDrawer.this, ProfileActivity.class);
                    startActivityForResult(intent, 1);
                }
                drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isVisible = true;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    toolbar.setTitle("Buku");
                    isVisible = true;
                } else if(isVisible) {
                    toolbar.setTitle("");
                    isVisible = false;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data.getStringExtra("status").equals("after_change_password")) {

        } else if (data.getStringExtra("status").equals("logout")) {
            finish();
        } else if (data.getStringExtra("status").equals("back_pressed")) {

        } else if(data.getStringExtra("status").equals("from_filter")){

        }
    }

    private void getBaseFragment(Fragment fragment, String title){
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame, fragment).commit();
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search_bar) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
