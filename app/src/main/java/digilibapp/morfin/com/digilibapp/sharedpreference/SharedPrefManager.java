package digilibapp.morfin.com.digilibapp.sharedpreference;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

import digilibapp.morfin.com.digilibapp.LoginActivity;
import digilibapp.morfin.com.digilibapp.models.Mahasiswa;

/**
 * Created by Morfin on 1/25/2018.
 */

public class SharedPrefManager {
    private static final String SHARED_PREF_NAME = "digilibsharedpref";
    private static final String KEY_ID_MAHASISWA = "keyidmahasiswa";
    private static final String KEY_NIM = "keynim";
    private static final String KEY_PASSWORD = "keypassword";
    private static final String KEY_NAMA = "keynama";
    private static final String KEY_JURUSAN = "keyjurusan";
    private static final String KEY_TOKEN = "keytoken";
    private static SharedPrefManager mInstance;
    private static Context mCtx;
    SharedPreferences sharedPreferences;
    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }
    public void onLogin(Mahasiswa mahasiswa) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_ID_MAHASISWA, mahasiswa.getId_mahasiswa());
        editor.putString(KEY_NIM, mahasiswa.getNim());
        editor.putString(KEY_PASSWORD, mahasiswa.getPassword());
        editor.putString(KEY_NAMA, mahasiswa.getNama());
        editor.putString(KEY_JURUSAN, mahasiswa.getJurusan());
        editor.putString(KEY_TOKEN, mahasiswa.getToken_android());
        editor.apply();
    }
    public Mahasiswa getMahasiswa(Mahasiswa mahasiswa) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new Mahasiswa(
                sharedPreferences.getString(KEY_ID_MAHASISWA, null),
                sharedPreferences.getString(KEY_NIM, null),
                sharedPreferences.getString(KEY_PASSWORD, null),
                sharedPreferences.getString(KEY_NAMA, null),
                sharedPreferences.getString(KEY_JURUSAN, null),
                sharedPreferences.getString(KEY_TOKEN, null)
        );
    }
    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_ID_MAHASISWA, null) != null;
    }
    public Map<String, String> showSession(){
        sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        Map<String, String> map = new HashMap<String, String>();
        map.put("ID_MAHASISWA", sharedPreferences.getString(KEY_ID_MAHASISWA,null));
        map.put("NIM", sharedPreferences.getString(KEY_NIM,null));
        map.put("PASSWORD", sharedPreferences.getString(KEY_PASSWORD,null));
        map.put("NAMA", sharedPreferences.getString(KEY_NAMA,null));
        map.put("JURUSAN", sharedPreferences.getString(KEY_JURUSAN,null));
        map.put("TOKEN", sharedPreferences.getString(KEY_TOKEN,null));
        return map;
    }
    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        Intent intent = new Intent(mCtx, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mCtx.startActivity(intent);
    }
}
